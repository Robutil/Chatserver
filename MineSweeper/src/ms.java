import java.util.Scanner;

public class ms {

    private static int[] parseInput(String input) throws NumberFormatException {
        String[] split = input.split(" ");
        int numb[] = new int[split.length];


        for (int i = 0; i < split.length; i++) {
            numb[i] = Integer.parseInt(split[i]);
        }

        return numb;
    }

    public static void main(String[] args) {
        int size = 0, mines = 0, wins = 0, losses = 0;
        int playcount = 0, totalmines_cleared = 0;

        try {
            if (args.length > 0) {
                size = parseInput(args[0])[0];
                if (args.length > 1) {
                    mines = parseInput(args[1])[0];
                }
            }
        } catch (NumberFormatException ex) {
            System.out.println("Usage: \n\t[size] \t\t-> Size of the minefield \n\t[number] \t-> Amount of mines in the field");
            return;
        }

        Scanner input = new Scanner(System.in);

        MineSweeper ms;

        if (size != 0 && mines != 0) ms = new MineSweeper(size, mines);
        else if (size != 0) ms = new MineSweeper(size, mines);
        else ms = new MineSweeper();

        if (size == 0) size = 6; //Default
        if (mines == 0) mines = 6; //Default

        System.out.println("Size: " + size + " Mines: " + mines);

        while (true) {
            System.out.println(ms.show_field());

            System.out.print("New guess: ");
            String userInput = input.nextLine();

            if (userInput.equals("Exit")) break;

            ms.guess(userInput);

            if (ms.getDone()) {
                playcount++;
                totalmines_cleared += ms.getCounter();
                if (ms.getWin()) {
                    wins++;
                } else {
                    losses++;
                }
                System.out.println("Statistics: \n\t" + "Play count: " + playcount
                        + "\n\tWins: " + wins
                        + "\n\t Losses: " + losses
                        + "\n\tTotal mines cleared: " + totalmines_cleared + "\n\tNew game? (y/n)");
                if (input.nextLine().equals("y")) {
                    System.out.print("New parameters? (n/param)");
                    String newgame = input.nextLine();
                    if (newgame.equals("n")) {
                        ms = new MineSweeper(size, mines);
                    } else {
                        try {
                            int newparam[] = parseInput(newgame);
                            if (newparam.length > 0) {
                                size = newparam[0];
                                if (newparam.length > 1) {
                                    mines = newparam[1];
                                }
                            }
                        } catch (NumberFormatException ex) {
                            System.out.println("Usage: \n\t[size] \t\t-> Size of the minefield \n\t[number]" +
                                    " \t-> Amount of mines in the field\n Shutting down.");
                            return;
                        }

                    }
                }
            }
        }
        System.out.println("Thanks for playing!");
    }
}
