import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Connect implements Runnable {

    /* === Variables === */

    private String hostname = "localhost";
    private int port = 2020;
    private Module module;
    private Thread active_thread;
    private String handle;
    private String abbreviation;
    private int id = 0;

    private Socket client_socket;
    private DataOutputStream outToServer;
    private BufferedReader recMes;


    /* === Constructors === */

    public Connect(Module module, String handle, String abbreviation) {
        this.module = module;
        this.handle = handle;
        this.abbreviation = abbreviation;
    }

    public Connect(Module module, String handle, String abbreviation, int port) {
        this.module = module;
        this.handle = handle;
        this.abbreviation = abbreviation;
        this.port = port;
    }

    public Connect(Module module, String handle, String abbreviation, String hostname) {
        this.module = module;
        this.handle = handle;
        this.abbreviation = abbreviation;
        this.hostname = hostname;
    }

    public Connect(Module module, String handle, String abbreviation, String hostname, int port) {
        this.module = module;
        this.handle = handle;
        this.abbreviation = abbreviation;
        this.hostname = hostname;
        this.port = port;
    }


    /* === Functions === */

    public void toServer() throws IOException {
        //Does module initialisation and server handshake

        client_socket = new Socket(hostname, port);
        outToServer = new DataOutputStream(new BufferedOutputStream(client_socket.getOutputStream()));
        DataInputStream inFromServer = new DataInputStream(client_socket.getInputStream());
        recMes = new BufferedReader(new InputStreamReader(inFromServer));

        //Basic handshake
        outToServer.writeBytes("[BOT] " + handle);
        outToServer.flush();
        id = inFromServer.read(); //personal id
        //TODO: htonl id

        //Create new channel
        outToServer.writeBytes("/n " + handle + "\n");
        outToServer.flush();
        String new_channel_message = recMes.readLine();
        System.out.printf(new_channel_message);

        //Join new channel
        //TODO: this

        module.init(this);
    }

    public void send_message(String message) {
        //Sends message to server
        System.out.println("Trying to send: " + message);
        try {
            outToServer.writeBytes("1:" + message + "\n");
            outToServer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean start_thread() {
        //Returns false if the thread is already running
        if (active_thread.isAlive()) return false;
        active_thread = new Thread();
        active_thread.run();
        return active_thread.isAlive();
    }

    @Override
    public void run() {
        //Receive handler
        String inc;
        while (true) {
            try {
                inc = recMes.readLine();
                if(inc == null || inc.length() < 1) break; //Server disconnected
                if(inc.contains(handle) && inc.contains("k")) break; //Kill signal
                if (abbreviation.equals("") ^ abbreviation == null) {
                    module.receive_handler(inc);
                } else if (inc.contains("/" + abbreviation)) {
                    String split[] = inc.split("/" + abbreviation + " ", 2);
                    module.receive_handler(split[1]);
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
    }
}
