public class Handler implements Module {
    private Connect connect; //!
    private MineSweeper mineSweeper;

    @Override
    public void init(Connect connect) {
        this.connect = connect;
        this.mineSweeper = new MineSweeper();
        connect.send_message(mineSweeper.show_field());
    }

    @Override
    public void receive_handler(String message) {
        System.out.println("Got message: " + message);
        if (message.contains("new")) {
            mineSweeper = new MineSweeper();
            connect.send_message(mineSweeper.show_field());
        } else {
            String answer = mineSweeper.guess(message);
            if (answer.contains("died")) {
                connect.send_message(answer + "\n" + mineSweeper.show_field_debug());
                mineSweeper = new MineSweeper();
            } else {
                connect.send_message(answer);
            }
        }
    }

    @Override
    public void run() {
        //Does nothing
    }
}
