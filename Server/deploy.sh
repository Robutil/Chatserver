#!/usr/bin/env bash
    
if [ -z "$1" ]; 
	then 
	echo Usage: $0 ip-address
        exit

fi
HOST=$1

USER=pi
DEST=/home/pi/Documents/Chatserver/Server

echo Connecting to $USER on $HOST

scp -rp . $USER@$HOST:$DEST

#ssh -T $USER@$HOST << EOF

echo Done
