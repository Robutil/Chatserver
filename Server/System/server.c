/*
 * server.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: This server creates a socket and accepts clients.
 * The clients are then polled using synchronous I/O multiplexing.
 * When a client is accepted, the client does not have the necessary
 * rights to perform a Chat-server command, e.g. chat. Only after
 * registration is this possible. If the client does not use the
 * expected protocol, the client is removed from the client stack.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <netdb.h>

#include "server.h"

#include "Artefact/client_store.h"
#include "Artefact/linkedList.h"

#include "../Network/layer.h"
#include "../Utilities/robutil.h"

#define PORT_DEF "52020"

//Global variables for cleanup purposes
static int server_sock = 0, cleanup = 0;

void sig_handler(int sig) {
    /* Manually abort server */

    if (sig == SIGINT) {
        r_log("Closing sockets... ");
        cleanup = 1;
        close(server_sock);
        exit(0);
    } else {
        r_error("SIG");
    }
}

int client_handler(int socket, uint16_t port) {
    /* Initialises a client with no rights */

    r_debug_val(VERBOSE, "Started client_handler", socket);

    //Create a new Account container, fill with default values
    struct Account *new_client = malloc(sizeof(struct Account));
    new_client->authentication = -1;
    new_client->channel = arrayList_init();
    bzero(new_client->client_handle, HANDLE_SIZE);
    new_client->client_message_buffer = network_buffer_new();
    new_client->client_port = port;
    new_client->fd_port = socket;

    r_debug(VERBOSE, "Adding client to linked list");

    //Add the client to linked list
    int32_t client_id = client_add(new_client, NO_FLAGS);

    r_debug_val(VERBOSE, "Got id", client_id);
    r_debug(VERBOSE, "Adding client to client_store");

    //Hash client id so fd leads to linked list id
    int temp = client_store_add(socket, client_id);
    if(temp == R_ERROR) client_remove(client_id);

    r_debug_val(VERBOSE, "Client store success (expecting zero)", temp);
    r_debug(VERBOSE, "Adding client done");

    return client_id;
}

int main(int argc, char *argv[]) {
    char port_fin[6];
    struct sockaddr_in client_address;
    struct addrinfo data, *server_info;;
    fd_set fd_active, fd_read;

    //Get preferred port
    if (argc < 2) {
        printf("No port specified, using default: %s\n", PORT_DEF);
        strncpy(port_fin, PORT_DEF, strlen(PORT_DEF));
    } else {
        strncpy(port_fin, argv[1], strlen(argv[1]));
    }

    //Set socket flags
    memset(&data, 0, sizeof(data));
    data.ai_family = AF_INET;
    data.ai_socktype = SOCK_STREAM;
    data.ai_flags = AI_PASSIVE;

    /* Basic TCP server initialisation */

    if (getaddrinfo(NULL, port_fin, &data, &server_info) != 0) r_error("addressInfo");

    server_sock = socket(server_info->ai_family, server_info->ai_socktype, server_info->ai_protocol);

    if (server_sock < 0) r_error("Socket");

    if (bind(server_sock, server_info->ai_addr, server_info->ai_addrlen) == -1) r_error("bind");

    if (signal(SIGINT, sig_handler) == SIG_ERR) r_error("SIG");

    //Initialise the linked list
    if (clients_init() == -1) r_error("Root linked list");

    //Initialise hash mechanic
    int port_hash = client_store_init();
    if (port_hash == -1) r_error("Could not initialise client list");

    //Create the root node of the linked list
    struct Account server;
    strncpy(server.client_handle, "Global", 6);
    server.channel = arrayList_init();
    client_add(&server, NO_FLAGS);

    //Start listening on the server socket
    if (listen(server_sock, CLIENT_NUMB) < 0) r_error("listen");

    r_log("Waiting for clients");

    //Set synchronous I/O
    FD_ZERO(&fd_active);
    FD_SET(server_sock, &fd_active);

    int i;
    while (!cleanup) {

        //Set FD to check the server socket
        fd_read = fd_active;

        //Block until data
        if (pselect(FD_SETSIZE, &fd_read, NULL, NULL, NULL, NULL) < 0) r_error("pSelect");

        //At least one client has readable data, loop clients
        for (i = 0; i < FD_SETSIZE; i++) {

            //Check if a read call is possible on a client
            if (FD_ISSET (i, &fd_read)) {

                //Check if client is new
                if (i == server_sock) {

                    /* New connection */

                    //Accept
                    size_t malloc_size = sizeof(struct sockaddr_in);
                    int new = accept(server_sock, (struct sockaddr *) &client_address, (socklen_t *) &malloc_size);
                    if (new < 0) r_error("connect");

                    //Log
                    fprintf(stderr, "Server: connect from host %s, port %d.\n",
                            inet_ntoa(client_address.sin_addr),
                            ntohs(client_address.sin_port));

                    //Allocate FD
                    FD_SET(new, &fd_active);

                    //Link client, this handles hash too
                    client_handler(new, client_address.sin_port);

                    //Clean up
                    bzero(&client_address, sizeof(struct sockaddr_in)); //TODO: memset
                    r_debug(VERBOSE, "Waiting for triggers");

                } else if (layer_check_buffer(i) == R_ERROR) { /* Message from client */

                    /* If the message handler (layer_check_buffer) returns a negative value, the client should be removed */
                    r_debug(VERBOSE, "Removing client");

                    //Find linked list number
                    int32_t ll_numb = client_store_find(i);

                    r_debug_val(VERBOSE, "Found linked list number", ll_numb);

                    //Remove client from linked list
                    client_remove(ll_numb);

                    //Remove client from hash
                    client_store_remove(i);

                    r_debug_val(VERBOSE, "Client removed", i);

                    //Close client socket
                    close(i);

                    //Clear fd
                    FD_CLR(i, &fd_active);
                }
            }
        }
    }
    close(server_sock);
    return 0;
}
