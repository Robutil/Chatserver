/*
 * system.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: These functions check the validity of incoming messages
 * and have the ability to perform changes to the client structure.
 * Examples include, but are not limited by: creating a channel,
 * sending a private message and creating an announcement.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "system.h"

#include "../Utilities/robutil.h"
#include "../Network/format.h"
#include "../Network/layer.h"
#include "Artefact/linkedList.h"
#include "Artefact/client_store.h"

static void channel_handler(struct Account *target, int target_id, char *msg) {
    /* Sends the msg to all members of targets' list */

    if (target->channel->length == 0) return; //Valid message, no public

    int i;
    for (i = 0; i < target->channel->length; ++i) { //For-each member in the channel list

        //Send message unless target == requester (otherwise double message in req log)
        if (target->channel->key[i] != target_id){
            struct Account *temp = client_find(target->channel->key[i]);
            layer_write(temp->client_port, msg);
        }
    }
}

static void sys_targeted_message(struct Sys_Cont *req) {
    /* Message is directed to a single client, a "Whisper". */

    int msg_id; //Check var

    //Check if the message has a valid identification number
    if ((msg_id = unwrap_id(req->msg->buffer, 2, ' ')) == R_ERROR) goto unknown;

    //Find message target
    struct Account *target = client_find(msg_id);

    //Check if target exists
    if (!target) goto unknown;

    //Formatted message
    char *format_msg = calloc(req->msg->mem_size + HANDLE_SIZE + HANDLE_SIZE + 10, sizeof(char));
    sprintf(format_msg, "[Whisper](%d)%s: %s", req->req_id, req->acc->client_handle, (char *) req->msg->buffer);

    //Send to target
    layer_write(target->client_port, format_msg);

    //Clean up
    free(format_msg);

    unknown:
    layer_write(req->acc->client_port, "Unknown id.");
}

static void sys_new_channel(struct Sys_Cont *req) {
    /* Creates a new channel */

    //Create a new Account container, fill with default values
    struct Account *channel = malloc(sizeof(struct Account));
    channel->authentication = -1;
    channel->channel = arrayList_init();
    bzero(channel->client_handle, HANDLE_SIZE);
    memcpy(channel->client_handle, req->msg->buffer + 3, req->msg->length - 3);
    channel->client_message_buffer = network_buffer_new();
    channel->client_port = 0;
    channel->fd_port = 0;

    client_add(channel, NO_FLAGS);

    char *msg = calloc(BUFFER_SIZE, 1);

    sprintf(msg, "User (%d) %s created channel %s.", req->req_id, req->acc->client_handle, channel->client_handle);
    layer_write(req->acc->client_port, msg);

    free(msg);
}

static void sys_end_channel(struct Sys_Cont *req) {

    int i, channel_id = unwrap_id(req->msg->buffer, 3, '\n');

    if (channel_id == R_ERROR) goto unknown;

    struct Account *channel = client_find(channel_id);

    if (!channel) goto unknown;

    /* Send termination information to clients */

    char *msg = calloc(BUFFER_SIZE, 1);

    sprintf(msg, "User (%d) %s terminated channel %s.", req->req_id, req->acc->client_handle, channel->client_handle);
    channel_handler(channel, NO_FLAGS, msg);

    memset(msg, 0, BUFFER_SIZE);
    sprintf(msg, "/k:%d:", channel_id); //Client command
    channel_handler(channel, NO_FLAGS, msg);

    free(msg);

    /* Clean up channel */

    //Remove channel from all users
    for (i = 0; i < channel->channel->length; ++i) {
        struct Account *temp = client_find(channel->channel->key[i]);
        arrayList_remove(temp->channel, channel_id);
    }

    //Destroy client
    client_remove(channel_id);

    return;

    unknown:
    layer_write(req->acc->client_port, "Unknown channel id");
}

void sys_join_channel(struct Sys_Cont *req) {
    /* Adds client to requested channel */

    int i, rejoin = 0, channel_id = unwrap_id(req->msg->buffer, 3, '\n');

    r_debug_val(VERBOSE, "Found channel id", channel_id);

    if (channel_id == R_ERROR) goto unknown;

    //Find the channel
    struct Account *channel = client_find(channel_id);

    if (!channel) goto unknown;

    if (channel->channel->length > 0) {
        for (i = 0; i < channel->channel->length; ++i) {
            if (channel->channel->key[i] == req->req_id) rejoin = 1;
        }
    }

    if (!rejoin) {
        arrayList_add(channel->channel, req->req_id);
        char *msg = calloc(BUFFER_SIZE, 1);
        sprintf(msg, "User (%d) %s joined channel %s.", req->req_id, req->acc->client_handle, channel->client_handle);

        //Announce joining
        channel_handler(channel, NO_FLAGS, msg);
        free(msg);

    } else layer_write(req->acc->client_port, "Already in channel.");

    return;

    unknown:
    layer_write(req->acc->client_port, "Unknown channel.");
}

void sys_leave_channel(struct Sys_Cont *req) {
    /* Removes client from requested channel */

    int channel_id = unwrap_id(req->msg->buffer, 3, '\n');

    if (channel_id != R_ERROR) {

        //Find the channel
        struct Account *channel = client_find(channel_id);
        if (!channel) goto unknown;

        char *msg = calloc(BUFFER_SIZE, 1);
        sprintf(msg, "User (%d) %s left channel %s.", req->req_id, req->acc->client_handle, channel->client_handle);

        //Announce leaving
        channel_handler(channel, NO_FLAGS, msg);
        free(msg);

        //Remove client from channel list
        arrayList_remove(channel->channel, req->req_id);

        //Also remove channel from user channel list
        arrayList_remove(req->acc->channel, channel_id);
    } else {
        unknown:
        layer_write(req->acc->client_port, "Unknown channel id.");
    }
}

static void sys_show_clients(struct Sys_Cont *req) {
    /* Shows clients in active channel */

    int channel_id, i;

    //Unwrap channel id
    if ((channel_id = unwrap_id(req->msg->buffer, 2, ':')) == -1) goto unknown;

    //Find the channel
    struct Account *recp = client_find(channel_id);
    if (!recp) goto unknown;

    //Create format buffers
    char *format = calloc(BUFFER_SIZE, 1);
    char *helper = calloc(BUFFER_SIZE, 1);

    /* Create a proper message that lists all users in the channel */

    sprintf(helper, "Current users:\n");

    for (i = 0; i < recp->channel->length; ++i) {

        struct Account *temp = client_find(recp->channel->key[i]);

        sprintf(format, "%s(%d) %s\n", helper, client_store_find(temp->fd_port), temp->client_handle);

        memset(helper, NO_FLAGS, BUFFER_SIZE);
        memcpy(helper, format, strlen(format));
    }

    //Send neat list to client
    layer_write(req->acc->client_port, format);

    //Clean up
    free(format);
    free(helper);

    return;

    unknown:
    layer_write(req->acc->client_port, "Unknown channel.");
}

static int sys_command(struct Sys_Cont *req) {
    /* This function checks if the requested message requires interaction
     * with client and channel structures. Commands are preceded by a '/' */

    //Check first character for '/'
    char *command = req->msg->buffer;
    if (*command != '/') return 0;

    //Get the second character to find requested command
    command = req->msg->buffer + 1;

    //If the second character is a number it is a target message
    if (isdigit(*command)) *command = 'd';

    r_debug(VERBOSE, "Got a system command");

    //Check which command is requested
    switch (*command) { //Read the debug statements for info per case
        case 'd':
            r_debug(VERBOSE, "Targeted message");
            sys_targeted_message(req);
            break;

        case 's':
            r_debug(VERBOSE, "Show clients.");
            sys_show_clients(req);
            break;

        case 'n':
            r_debug(VERBOSE, "New channel.");
            sys_new_channel(req);
            break;

        case 'j':
            r_debug(VERBOSE, "Join channel.");
            sys_join_channel(req);
            break;

        case 'l':
            r_debug(VERBOSE, "Leave channel.");
            sys_leave_channel(req);
            break;

        case 'k':
            r_debug(VERBOSE, "End channel.");
            sys_end_channel(req);
            break;

        default:
            r_debug(VERBOSE, "Unknown.");
    }
    return 1;
}

int new_client(struct Sys_Cont *req) {
    /* Handles unauthenticated clients */

    r_debug(VERBOSE, "Authenticating client");

    if (req->acc->authentication != -1) return 0;

    strncpy(req->acc->client_handle, req->msg->buffer, req->msg->length);
    r_debug(VERBOSE, "Found name: ");
    r_debug(VERBOSE, req->acc->client_handle);

    req->acc->authentication = 0;

    struct Network_Buffer *id = network_buffer_new();
    network_write_int(id, req->req_id);

    layer_write_safe(req->acc->fd_port, id);

    return 1;
}

void sys_message(struct Sys_Cont *req) {
    /* Finds out message contents and acts accordingly */

    r_debug(VERBOSE, "Starting sys_message");

    if (new_client(req)) return;

    int msg_id; // Checking var

    //Check if it's a sys call
    if (sys_command(req)) return;

    r_debug(VERBOSE, "Not a sys_command. Message: ");
    r_debug(VERBOSE, req->msg->buffer);

    /* If it's not a sys call it is a targeted message, either
     * to a client or a channel. In this case the message is
     * preceded by the targets' identification number */

    //Check if the message has a valid identification number
    if ((msg_id = unwrap_id(req->msg->buffer, NO_FLAGS, ':')) == R_ERROR) goto unknown;

    //Find message target
    struct Account *target = client_find(msg_id);

    //Check if target exists
    if (!target) goto unknown;

    /* If the target does not have a valid port number the target
     * is a channel. Create a properly formatted message and
     * announce it on the channel. This is the only option if the
     * requested message was not a system command */

    if (target->client_port == 0) {

        //Formatted message
        char *format_msg = calloc(req->msg->mem_size + HANDLE_SIZE + HANDLE_SIZE + 10, sizeof(char));
        sprintf(format_msg, "[%s](%d)%s: %s", target->client_handle, req->req_id, req->acc->client_handle,
                (char *) req->msg->buffer);

        //Send to all clients in the targeted channel
        channel_handler(target, req->req_id, format_msg);

        //Clean up format buffer
        free(format_msg);

        //Done
        return;
    }

    unknown:
    r_debug(VERBOSE, "UNKNOWN");
    layer_write(req->acc->fd_port, "Unknown request.");
    return; //Not required here
}