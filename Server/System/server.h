#ifndef SERVER_H
#define SERVER_H

/*
 * server.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: This server creates a socket and accepts clients.
 * The clients are then polled using synchronous I/O multiplexing.
 * When a client is accepted, the client does not have the necessary
 * rights to perform a Chatserver command, e.g. chat. Only after
 * registration is this possible. If the client does not use the
 * expected protocol, the client is removed from the client stack.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#define CLIENT_NUMB 64 //Maximum amount of clients
#define BUFFER_SIZE 1024 //Buffer size used in receiving and sending messages
#define HANDLE_SIZE 60 //Maximum length of a client name
#define SERVER_SIDE

//Readability
#define NO_FLAGS 0
#define R_ERROR -1
#define VERBOSE 1 //Debug

//Required includes
#include <stdint.h>
#include "Artefact/arrayList.h"

/* The following struct represents a client or channel in the
 * Chatserver log. */

struct Account {
    int authentication; //-1 means the client is unregistered
    int points; //Moderating system
    uint16_t client_port; //Used for sending notifications from server
    int fd_port; //Used for verification
    char client_handle[HANDLE_SIZE]; //Clients name in chat log
    struct ArrayList *channel; //All the channels that the client occupies
    struct Network_Buffer *client_message_buffer; //Receive buffer
};

#endif
