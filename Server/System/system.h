#ifndef SYSTEM_H
#define SYSTEM_H

/*
 * system.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: These functions check the validity of incoming messages
 * and have the ability to perform changes to the client structure.
 * Examples include, but are not limited by: creating a channel,
 * sending a private message and creating an announcement.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

//Dependencies for structs
#include "server.h"
#include "../Network/network.h"

//Minimal info required to perform functions
struct Sys_Cont{
    struct Account *acc; //Reference to requesting client
    struct Network_Buffer *msg; //Command or message from client
    int req_id; //Id of requester
};

/* Finds out message contents and acts accordingly */
void sys_message(struct Sys_Cont *req);

#endif
