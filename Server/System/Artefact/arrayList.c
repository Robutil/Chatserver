/*
 * arrayList.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: This is an implementation of an array list in C.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdlib.h>

#include "arrayList.h"

/*=== ArrayList ===*/

struct ArrayList *arrayList_init() {
    //Returns an empty but usable ArrayList

    struct ArrayList *new_list = malloc(sizeof(struct ArrayList));
    new_list->length = 0;
    new_list->key = NULL;
    return new_list;
}

void arrayList_add(struct ArrayList *list, int value) {
    //Adds an int to the ArrayList

    int *swap, i;
    list->length++;
    swap = malloc(list->length * 4);
    for (i = 0; i < list->length - 1; ++i) {
        swap[i] = list->key[i];
    }
    swap[i] = value;
    if (list->key != NULL) free(list->key);
    list->key = swap;
}

int arrayList_remove(struct ArrayList *list, int value) {
    //No duplicate keys allowed, returns new length or -1 on failure

    int *swap, i, counter = 0, flag = 0;
    if (list->length == 0) return -1;
    else if (list->length == 1) {
        if (list->key[0] != value) return -1;
        list->key = NULL;
        return (int) list->length--;
    } else {
        swap = malloc((list->length - 1) * 4);
        for (i = 0; i < list->length; ++i) {
            if (list->key[i] != value) {
                swap[counter] = list->key[i];
                counter++;
            } else {
                flag = 1;
            }
        }
        if (!flag) return -1;
        if (list->key != NULL) free(list->key);
        list->key = swap;
        list->length--;
        return (int) list->length;
    }
}

