/*
 * clients.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: These functions are used to link an fd number to a
 * linked list identification. There is limited amount of links,
 * and it is defined by a static value (CLIENT_NUMB).
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "client_store.h"
#include "../server.h"

static int *fd_number = NULL;

int client_store_init(){
    /* Reserves memory for maximum amount of clients. Will load user config
     * to restore previous Accounts. Returns -1 on failure. */

    fd_number = malloc(sizeof(int32_t) * CLIENT_NUMB);

    int i;
    for (i = 0; i < CLIENT_NUMB; ++i) {
        fd_number[i] = -1;
    }

    return 0;
    //TODO: Load config here
}

int client_store_add(int fd, int ll){
    /* Stores ll at fd. Returns -1 on failure */

    //Can not exceed the malloc
    if(fd > CLIENT_NUMB) return -1;

    //Check if fd is already active
    if(fd_number[fd] != -1) return -1;

    fd_number[fd] = ll;

    return 0;
}

int client_store_remove(int fd){
    /* Removes client and resets fd list. Returns -1 on failure. */

    //Cannot exceed malloc
    if(fd > CLIENT_NUMB) return -1;

    //Always successful
    fd_number[fd] = -1;
    return 0;
}

int client_store_find(int fd){
    return fd_number[fd];
}



