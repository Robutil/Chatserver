/*
 * list.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: Implementation of a linked list in C.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "linkedList.h"
#include "../../Utilities/robutil.h"

#define PARENT 1
#define ACTUAL 0

//Can assign own id's if needed
static int unique_id_counter = 0;

struct Node {
    struct Node *next_node;
    void *data; //Data can be anything
    int unique_id;
};

static struct Node *root;

/*=== Linked List===*/

static int linkedList_new_node(struct Node **node) {
    //Returns an empty, but usable Node
    *node = malloc(sizeof(struct Node));
    (*node)->next_node = 0;
    return (*node)->unique_id = 0;
}

static int linkedList_insert_Node(struct Node *node, void *info, int unique_id) {
    //Adds a client's info to a (new) node, returns local id
    if (node->unique_id == 0) {
        node->unique_id = unique_id;
        node->data = info;
        return node->unique_id;
    } else {
        if (node->next_node == 0) r_error_ld(linkedList_new_node(&node->next_node), "Node");
        return linkedList_insert_Node(node->next_node, info, unique_id);
    }
}

static struct Node *linkedList_find_r(int unique_id, int ret_parent) {
    //Returns NULL if client doesn't exist, flag returns parent

    struct Node *cycle;
    cycle = root;
    while (cycle->unique_id != unique_id) {
        if (cycle->next_node == 0) return NULL;
        if (ret_parent && cycle->next_node->unique_id == unique_id) return cycle;
        cycle = cycle->next_node;
    }
    return cycle;
}

void client_remove(int unique_id) {
    struct Node *kill_node, *parent_node = linkedList_find_r(unique_id, PARENT);
    kill_node = parent_node->next_node;
    parent_node->next_node = kill_node->next_node;
    free(kill_node);
}

void *client_find(int unique_id) {
    /* Returns the void* container linked to the unique id. Returns null on failure. */

    return linkedList_find_r(unique_id, ACTUAL)->data;
}

int client_add(void *info, int unique_id) {
    if(unique_id == 0) unique_id = ++unique_id_counter;

    //Returns unique_id
    return linkedList_insert_Node(root, info, unique_id);
}

int clients_init() {
    //Creates a starting point in memory, empty info but mandatory
    return linkedList_new_node(&root);
}
