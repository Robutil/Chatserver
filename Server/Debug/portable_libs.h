#ifndef PORTABLE_LIBS_H
#define PORTABLE_LIBS_H

#ifndef VERBOSE
#define VERBOSE 0
#endif

int layer_write(int client_port, char *message);

struct Network_Buffer* layer_read(int client_port, struct Network_Buffer *req);

#endif
