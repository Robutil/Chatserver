#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <pthread.h>

#include "portable_libs.h"
#include "../Network/format.h"
#include "../Network/network.h"
#include "../Utilities/robutil.h"
#include "../Utilities/sleep.h"

#define PORT_DEF "52020"
#define ADD_DEF "localhost"
#define BUFFER_SIZE 1024
#define HANDLE_SIZE 60

static int client_sock = -1;

void sig_handler(int sig) {
    if (sig == SIGINT) {
        r_log("Closing socket... ");
        if (client_sock > 0) close(client_sock);
        exit(0);
    } else {
        r_error("SIG");
    }
}

struct Client_info {
    char handle[HANDLE_SIZE];
    int sock_loc;
    int id;
    int active_channel;
    char *channel_name;
};

struct Mess_info {
    int id;
    char *raw;
    int som;
};

void kill_identifier(char *buffer, struct Client_info *temp) {
    struct Mess_info wrap;
    wrap.som = 3;
    wrap.raw = buffer;

    int wat = unwrap_id(wrap.raw, 0, ':');
    if (temp->active_channel == wrap.id) {
        bzero(buffer, BUFFER_SIZE);
        sprintf(buffer, "/j 1\n");
        write(temp->sock_loc, buffer, strlen(buffer));
    }
}

void channel_identifier(char *buffer, struct Client_info *temp) {
    struct Mess_info wrap;
    wrap.som = 3;
    wrap.raw = buffer;

    wrap.id = unwrap_id(wrap.raw, 0, ':');
    temp->active_channel = wrap.id;
    temp->channel_name = malloc(HANDLE_SIZE);
    bzero(temp->channel_name, HANDLE_SIZE);
    memcpy(temp->channel_name, wrap.raw, strlen(wrap.raw));

    printf("\rJoined channel [%d] %s.", temp->active_channel, temp->channel_name);
    int i;
    for (i = 0; i < 15; ++i) {
        printf(" ");
    }
    printf("\n");
}

void handle_server_command(char *buffer, struct Client_info *req) {
    if (buffer[0] != '/') return;
    char command = buffer[1];

    r_log("Server command");
    r_log(buffer);

    switch (command) {
        case 'k':
            //A channel was destroyed
            kill_identifier(buffer, req);
            break;
        case 'c':
            //Change active channel
            channel_identifier(buffer, req);
            break;
        default:
            r_error_soft("Unknown server command");
    }
}

void *receive_handler(void *param) {

    //Set client information
    struct Client_info *req = param;

    struct Network_Buffer *packet, *server_buffer = network_buffer_new();

    //Make sure we got a solid connection
    sleep_mil(1000);

    int i;
    char *proper_format = malloc(BUFFER_SIZE);

    while (1) {

        //Get data from server
        packet = layer_read(req->sock_loc, server_buffer);

        if (packet) {
            //r_debug(VERBOSE, "Received a full packet!");

            //Check if the message was a server command
            char *server_command = packet->buffer;
            if (*server_command == '/') { //Received server info
                handle_server_command(packet->buffer, req);
                break; //Just here to please compiler warnings
            }

            //Prep buffer
            bzero(proper_format, BUFFER_SIZE);

            //Create user input info line
            sprintf(proper_format, "\r[%s](%d) %s: ", req->channel_name, req->id, req->handle);

            //Reset console
            printf("\r");

            //Clear old user info line
            for (i = 0; i < strlen(proper_format) + 6; ++i) printf(" ");

            //Fill clean line with received data
            printf("\r%s\n", (char *) packet->buffer);

            //Flush orders
            fflush(stdout);

            //Reset user input info line
            printf("%s", proper_format);
            fflush(stdout);
        }
    }
}

char *format_message(struct Client_info *temp, char *buffer) {

    //Create message buffer
    char *format = malloc(BUFFER_SIZE);
    bzero(format, BUFFER_SIZE);

    if (buffer[0] != '/') {
        //Command to server
        sprintf(format, "%d:%s", temp->active_channel, buffer);

    } else if (buffer[1] == 's') {
        //Standard message
        sprintf(format, "/s%d:%s", temp->active_channel, &buffer[3]);
    }

    char *network = format;

    r_log(network);

    if (strlen(network) > 0) strncpy(buffer, network, strlen(network));

    free(network);
}

int main(int argc, char *argv[]) {
    struct addrinfo data, *server_info;
    struct Client_info client;
    int pivot;
    char buffer[BUFFER_SIZE], port_pref[6], add_pref[32];
    char message_buffer[BUFFER_SIZE + 6];

    //Preferred values
    memset(&data, 0, sizeof(data));
    memset(&client, 0, sizeof(client));
    bzero(client.handle, HANDLE_SIZE);
    data.ai_family = AF_INET;
    data.ai_socktype = SOCK_STREAM;
    data.ai_flags = AI_PASSIVE;

    //Get username
    if (argc >= 2) {
        strncpy(client.handle, argv[1], strlen(argv[1]));
    } else {
        r_error("Usage: <Handle> <Hostname> <Port>");
    }

    //Get address
    if (argc >= 3) {
        strncpy(add_pref, argv[2], strlen(argv[2]));
    } else {
        printf("No address specified, using default: %s\n", ADD_DEF);
        strncpy(add_pref, ADD_DEF, strlen(ADD_DEF));
    }
    if (argc == 4) {
        strncpy(port_pref, argv[3], strlen(argv[3]));
    } else {
        printf("No port specified, using default: %s\n", PORT_DEF);
        strncpy(port_pref, PORT_DEF, strlen(PORT_DEF));
    }

    if (getaddrinfo(add_pref, port_pref, &data, &server_info) != 0) r_error("addrinfo");
    client.sock_loc = socket(server_info->ai_family, server_info->ai_socktype, server_info->ai_protocol);

    if (client.sock_loc < 0) r_error("socket");

    client_sock = client.sock_loc;

    if (signal(SIGINT, sig_handler) == SIG_ERR) r_error("SIG");

    if (connect(client.sock_loc, server_info->ai_addr, server_info->ai_addrlen) < 0) r_error("connect");

    //Handshake
    pivot = layer_write(client.sock_loc, client.handle);

    if (pivot < 0) r_error("write");

    struct Network_Buffer *server_buffer = network_buffer_new();

    struct Network_Buffer *id_packet = NULL;

    while (!id_packet) id_packet = layer_read(client.sock_loc, server_buffer);

    r_debug(1, "Full message!");

    int *id_numb = id_packet->buffer;
    client.id = *id_numb;

    r_log_val("My id is", client.id);
    client.active_channel = 0;
//    client.channel_name = "Global";

//    if (temp.next == NULL) {
//        bzero(buffer, BUFFER_SIZE);
//        pivot = read(client.sock_loc, buffer, BUFFER_SIZE - 1);
//        r_log(buffer);
//        if (pivot < 0) r_error("read");
//        bzero(temp.format, BUFFER_SIZE);
//        strncpy(temp.format, buffer, strlen(buffer));
//        unwrap_packet(&temp);
//        channel_identifier(temp.raw, &client);
//    } else {
//        unwrap_packet(temp.next);
//        channel_identifier(temp.next->raw, &client);
//        clear_packet(temp.next);
//    }

    pthread_t thread_recv;
    pthread_create(&thread_recv, NULL, receive_handler, &client);

    /* The following loop checks stdin for input and sends this input to the server. */
    while (1) {
        //Send data back and forth (testing purposes)

        //Prep header for user input
        bzero(buffer, BUFFER_SIZE);
        bzero(message_buffer, BUFFER_SIZE + 6);
        sprintf(message_buffer, "[%s](%d) %s: ", client.channel_name, client.id, client.handle);

        //Write user header to own console
        printf("%s", message_buffer);
        fflush(stdout);

        //Get user message
        fgets(buffer, BUFFER_SIZE - 1, stdin);
        if (buffer[0] == 'q') break;

        //Send message to server
        format_message(&client, buffer);
        layer_write(client.sock_loc, buffer);

        if (pivot < 0) r_error("ERROR writing to socket");
    }
    close(client.sock_loc);
    return 0;
}