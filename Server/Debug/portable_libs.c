/*
 * portable_libs.h, 8/17/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: -
 * Summary: -
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT 
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "portable_libs.h"

#include "../Utilities/robutil.h"
#include "../Network/network.h"

int layer_write(int client_port, char *message) {
    /* This function makes sure the message is a proper
     * string and sends it to the requested target */

    r_debug(VERBOSE, "Starting write");
    r_debug(VERBOSE, message);

    //Obligatory check to avoid crash
    if ((client_port <= 0) || !message) return -1;

    r_debug(VERBOSE, "Getting message length");

    //Assign length to a variable due to excessive use
    size_t mess_len = strlen(message);

    r_debug(VERBOSE, "Creating network packet");

    struct Network_Buffer *msg = network_buffer_new();
    int temp = htonl((uint32_t) strlen(message));
    network_write_int(msg, temp);
    network_write_string(msg, message, strlen(message));

    if (message[mess_len - 1] != '\n') network_write_string(msg, "\n", 2);

    r_debug(VERBOSE, "Sending network packet");

    //Send the message to the client
    return (int) write(client_port, msg->buffer, msg->length);
}

struct Network_Buffer *layer_read(int client_port, struct Network_Buffer *req) {
    /* Checks if there are any full packets in the client buffer */

    int received_bytes = (int) read(client_port, req->buffer + req->pivot, network_get_available(req));
    if (received_bytes <= 0) {
        r_debug(VERBOSE, "Failed to do a receive!");
        exit(0); //TODO: more gracefully
    }

    req->pivot += received_bytes;
    req->length += received_bytes;

    r_debug_val(VERBOSE, "Received bytes", received_bytes);

    int save = req->pivot;
    req->pivot = 0;

    //Read protocol. This function implements a mem copy (fixme)
    struct Network_Buffer *ret = network_buffer_read_protocol_safe(req, 0);

    if (ret) {
        req->pivot = (unsigned int) req->length;
        network_buffer_clean(req);
    }
    else req->pivot = (unsigned int) save;

    return ret;
}