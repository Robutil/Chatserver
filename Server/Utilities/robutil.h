#ifndef ROBUTIL_H
#define ROBUTIL_H

/*
 * robutil.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: Mostly debugging functions.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */


void r_error(char *mess);

void r_error_soft(char *mess);

void r_error_ld(int pivot, char *mess);

void r_error_ld_soft(int pivot, char *mess);

void r_log(char *mess);

void r_debug(int req, char *mess);

void r_debug_val(int req, char *mess, int val);

void r_log_val(char *mess, int val);

void r_clear(char *mess);

#endif

//ps aux
//kill PID
