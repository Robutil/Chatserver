#ifndef LAYER_H
#define LAYER_H

/*
 * layer.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: This layer keeps check of client buffers and states.
 * If a client is illegal, per authentication or just a disconnect,
 * the client is removed. It also checks the client buffer for
 * completed messages or commands and passes those on to system.c.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include "network.h"

/* This function performs a read call on the given descriptor,
 * and checks for illegal clients. When a client is faulty it
 * will return R_ERROR. Protocol: [message length][message] */

int layer_check_buffer(int client_sock);

/* This function transmits a message to the specified port according
 * to the network protocol */

int layer_write(int client_port, char *message);

void layer_write_safe(int client_port, struct Network_Buffer *req);

#endif
