/*
 * network.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: These functions handle network data. Keep in mind that
 * excessive use of these functions might be slow considering
 * the memory is copied into new buffers to ensure data integrity.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>

#include "network.h"
#include "../Utilities/robutil.h"

struct Network_Buffer *network_buffer_new_limited(size_t buffer_size) {
    /* Create a buffer with other than standard memory size.
     * Use with care, preferably use network_buffer_new(). */

    struct Network_Buffer *reference = malloc(sizeof(struct Network_Buffer));
    reference->buffer = malloc(buffer_size);
    memset(reference->buffer, 0, buffer_size);

    reference->length = 0;
    reference->pivot = 0;
    reference->mem_size = buffer_size;

    return reference;
}

struct Network_Buffer *network_buffer_new() {
    /* Creates a new network buffer */

    return network_buffer_new_limited(NETWORK_BUFFER_SIZE);
}

void network_buffer_destroy(struct Network_Buffer *req) {
    /* Frees memory, do not use req after calling this */

    free(req->buffer);
    free(req);

    req = NULL;
}

void network_buffer_clear(struct Network_Buffer *req) {
    /* Resets buffer contents to default. This only works when
     * the buffer has NETWORK_BUFFER_SIZE size. */

    memset(req->buffer, NO_FLAGS, NETWORK_BUFFER_SIZE);
    bzero(req->buffer, NETWORK_BUFFER_SIZE); //Obsolete
    req->length = 0;
    req->pivot = 0;
}

void network_buffer_clean(struct Network_Buffer *req) {
    /* Removes all data before pivot. Brings rest to start of buffer */

    if (req->pivot == 0) return; //Nothing to do

    memmove(req->buffer, req->buffer + req->pivot, req->length - req->pivot);
    req->length -= req->pivot;
    req->pivot = 0;
}

void network_buffer_append(struct Network_Buffer *base, struct Network_Buffer *req) {
    /* Adds req buffer to base buffer. The pivot will not be moved if this operation fails */

    if ((base->pivot + req->length) > base->mem_size) return;

    memcpy(base->buffer + base->pivot, req->buffer + req->pivot, req->length - req->pivot);

    base->length += req->length;
    base->pivot += req->length;
}

void network_buffer_ataph(struct Network_Buffer *req) {
    /*Puts pivot at 0 */

    req->pivot = 0;
}

void network_buffer_lock(struct Network_Buffer *req) {
    /* Puts pivot at the end of content */

    req->pivot = (unsigned int) req->length;
}

size_t network_get_length(struct Network_Buffer *req) {
    /* Returns length of data after pivot. */

    return req->length - req->pivot;
}

void *network_read(struct Network_Buffer *req, size_t size) {
    /* Reads size bit integer from req. The pivot will not be moved if this operation fails */

    if (req->pivot + size > req->length) return 0;

    //Allocate memory
    void *res = calloc(size, 1);

    //Transfer size to newly allocated memory
    memcpy(res, req->buffer + req->pivot, size);

    //Move pivot for future use
    req->pivot += size;

    return res;
}

int8_t network_read_int8(struct Network_Buffer *req) {
    /* Reads 8 bit integer from req. The pivot will not be moved if this operation fails */

    if (req->pivot + sizeof(int8_t) > req->length) return 0;

    int8_t res;
    memcpy(&res, req->buffer + req->pivot, sizeof(int8_t));
    req->pivot += sizeof(int8_t);

    return res;
}

int32_t network_read_int32(struct Network_Buffer *req) {
    /* Reads 32 bit integer from req. The pivot will not be moved if this operation fails */

    if (req->pivot + sizeof(int32_t) > req->length) return 0;

    int32_t res;
    memcpy(&res, req->buffer + req->pivot, sizeof(int32_t));
    req->pivot += sizeof(int32_t);

    return res;
}

int64_t network_read_int64(struct Network_Buffer *req) {
    /* Reads 64 bit integer from req. The pivot will not be moved if this operation fails */

    if (req->pivot + sizeof(int64_t) > req->length) return 0;

    int64_t res;
    memcpy(&res, req->buffer + req->pivot, sizeof(int64_t));
    req->pivot += sizeof(int64_t);

    return res;
}

char *network_read_string(struct Network_Buffer *req, size_t length) {
    /* Reads string of defined length from req. The pivot will not be moved if this operation fails */

    char *res = malloc(length + 1);
    if (req->pivot + length > req->length) return NULL;

    memcpy(res, req->buffer + req->pivot, length);
    res[length] = 0; //Null terminated string, according to posix

    req->pivot += length;
    return res;
}

struct Network_Buffer *network_read_data(struct Network_Buffer *req, size_t length) {
    /* Reads data for length and returns a new struct Network_Buffer. Zero reads all */

    if (req->pivot + length > req->length) return NULL;
    if (length == 0) length = req->length - req->pivot;

    struct Network_Buffer *data = network_buffer_new_limited(length);

    data->length = length;

    memcpy(data->buffer, req->buffer + req->pivot, length);

    req->pivot += length;

    return data;
}

struct Network_Buffer *network_buffer_read_protocol_safe(struct Network_Buffer *req, int do_htonl) {
    /*Protocol int32 packet size + content*/

    r_debug(VERBOSE, "Reading protocol");

    if (req->pivot + sizeof(int32_t) > req->length) return 0;

    //Extract packet length
    int32_t *res = malloc(sizeof(int32_t));

    memcpy(res, req->buffer + req->pivot, sizeof(int32_t));

    if (do_htonl) *res = ntohl((uint32_t) *res);

    r_debug_val(VERBOSE, "Testing packet length", *res);

    if (req->pivot + sizeof(int32_t) + *res > req->length) {
        //Buffer does not contain the full message

        free(res);
        return NULL;

    } else {
        //Got a full packet

        //Move pivot past packet size integer
        req->pivot += sizeof(int32_t);

        //Read packet contents in new buffer
        struct Network_Buffer *ret = network_read_data(req, (size_t) *res);

        free(res);
        return ret;
    }
}

struct Network_Buffer *network_buffer_read_protocol(struct Network_Buffer *req) {
    return network_buffer_read_protocol_safe(req, 1);
}

void network_write(struct Network_Buffer *req, void *data, size_t length) {
    /* Writes data to req. The pivot will not be moved if this operation fails */

    if (req->pivot + length > NETWORK_BUFFER_SIZE) return;

    memcpy(req->buffer + req->pivot, data, length);

    req->length += length;
    req->pivot += length;
}

void network_write_int16(struct Network_Buffer *req, int32_t value) {
    /* Writes int_32 to req. The pivot will not be moved if this operation fails */

    if (req->pivot + sizeof(int16_t) > NETWORK_BUFFER_SIZE) return;

    memcpy(req->buffer + req->pivot, &value, sizeof(int16_t));
    req->length += sizeof(int16_t);
    req->pivot += sizeof(int16_t);
}

void network_write_int(struct Network_Buffer *req, int32_t value) {
    /* Writes int_32 to req. The pivot will not be moved if this operation fails */

    if (req->pivot + sizeof(int32_t) > NETWORK_BUFFER_SIZE) return;

    memcpy(req->buffer + req->pivot, &value, sizeof(int32_t));
    req->length += sizeof(int32_t);
    req->pivot += sizeof(int32_t);
}

void network_write_int64(struct Network_Buffer *req, int64_t value) {
    /* Writes int_64 to req. The pivot will not be moved if this operation fails */

    if (req->pivot + sizeof(int64_t) > NETWORK_BUFFER_SIZE) return;

    memcpy(req->buffer + req->pivot, &value, sizeof(int64_t));
    req->length += sizeof(int64_t);
    req->pivot += sizeof(int64_t);
}

void network_write_string(struct Network_Buffer *req, const char *req_string, size_t length) {
    /* Writes string of specified length to req. The pivot will not be moved if this operation fails */

    if (req->pivot + length > req->mem_size) return;

    memcpy(req->buffer + req->pivot, req_string, length);

    req->pivot += length;
    req->length += length;
}

size_t network_get_available(struct Network_Buffer *req) {
    return req->mem_size - req->length;
}