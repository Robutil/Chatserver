#ifndef NETWORK_H
#define NETWORK_H

/*
 * network.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: These functions handle raw data. Keep in mind that
 * excessive use of these functions might be slow considering
 * the memory is copied into new buffers to ensure data integrity.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#define NETWORK_BUFFER_SIZE 4096

struct Network_Buffer {
    size_t mem_size;
    void *buffer;
    size_t length;
    unsigned int pivot;
};

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "../System/server.h"

struct Network_Buffer *network_buffer_new();

void network_buffer_destroy(struct Network_Buffer *req);

void network_buffer_clear(struct Network_Buffer *req);

void network_buffer_clean(struct Network_Buffer *req);

void network_buffer_append(struct Network_Buffer *base, struct Network_Buffer *req);

void network_buffer_ataph(struct Network_Buffer *req);

void network_buffer_lock(struct Network_Buffer *req);

size_t network_buffer_length(struct Network_Buffer *req);

int32_t network_read_int(struct Network_Buffer *req, int peek);

char *network_read_string(struct Network_Buffer *req, size_t length);

struct Network_Buffer *network_read_data(struct Network_Buffer *req, size_t length);

struct Network_Buffer *network_buffer_read_protocol(struct Network_Buffer *req);

struct Network_Buffer *network_buffer_read_protocol_safe(struct Network_Buffer *req, int do_htonl);

void network_write_int(struct Network_Buffer *req, int32_t value);

void network_write_string(struct Network_Buffer *req, const char *req_string, size_t length);

size_t network_get_available(struct Network_Buffer *req);

#endif
