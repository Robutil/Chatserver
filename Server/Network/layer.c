/*
 * layer.h, 6/11/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 * Summary: This layer keeps check of client buffers and states.
 * If a client is illegal, per authentication or just a disconnect,
 * the client is removed. It also checks the client buffer for
 * completed messages or commands and passes those on to system.c.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <string.h>
#include <stdio.h>
#include <sys/unistd.h>
#include <stdlib.h>

#include "layer.h"

#include "../Utilities/robutil.h"
#include "../System/system.h"

#include "../System/Artefact/client_store.h"
#include "../System/Artefact/linkedList.h"

void layer_write_safe(int client_port, struct Network_Buffer *req) {
    /* This function can be performed while handling binary data */

    r_debug(VERBOSE, "Starting safe write");
    size_t data_size = req->length;

    memmove(req->buffer + 4, req->buffer, req->length);

    req->pivot = 0;

    network_write_int(req, (int32_t) data_size);

    write(client_port, req->buffer, req->length + sizeof(uint32_t));
    r_debug(VERBOSE, "Ended safe write");
}

int layer_write(int client_port, char *message) {
    /* This function makes sure the message is a proper
     * string and sends it to the requested target */

    r_debug(VERBOSE, "Starting write");
    r_debug(VERBOSE, message);

    //Obligatory check to avoid crash
    if ((client_port <= 0) || !message) return -1;

    r_debug(VERBOSE, "Getting message length");

    //Assign length to a variable due to excessive use
    size_t mess_len = strlen(message);

    r_debug(VERBOSE, "Creating network packet");

    struct Network_Buffer *msg = network_buffer_new();
    network_write_int(msg, (int32_t) strlen(message));
    network_write_string(msg, message, strlen(message));

    if (message[mess_len - 1] != '\n') network_write_string(msg, "\n", 2);

    r_debug(VERBOSE, "Sending network packet");

    //Send the message to the client
    return (int) write(client_port, msg->buffer, msg->length);
}

static int layer_read(struct Account *req, int client_id) {
    /* Checks if there are any full packets in the client buffer */

    //Reset buffer pivot
    network_buffer_ataph(req->client_message_buffer); //TODO: What? This will break when not a full msg

    //Read protocol. This function implements a mem copy (fixme)
    struct Network_Buffer *message = network_buffer_read_protocol(req->client_message_buffer);

    //Check if there is a full message, if not simply return
    if (message == NULL) {
        r_debug(VERBOSE, "No full packet");
        return 0;
    }

    r_log(message->buffer);

    //Create a struct with data system.c needs
    struct Sys_Cont *temp = malloc(sizeof(struct Sys_Cont));
    temp->acc = req;
    temp->msg = message;
    temp->req_id = client_id;

    //Pass the system struct, message validity is irrelevant
    sys_message(temp);

    r_debug(VERBOSE, "Layer cleaning up");

    //Clean up
    network_buffer_clean(req->client_message_buffer);
    free(message);
    free(temp);

    //Let layer_check_buffer know there might be more messages in the buffer
    return 1;
}

int layer_check_buffer(int client_sock) {
    /* This function performs a read call on the given descriptor,
     * and checks for illegal clients. When a client is faulty it
     * will return R_ERROR. Protocol: [message length][message] */

    r_debug(VERBOSE, "Layer check buffer start");

    //Find client_id in hash
    int32_t client_id = client_store_find(client_sock);
    if (client_id == -1) return -1; //Invalid client, disconnect

    r_debug_val(VERBOSE, "Found client_id", client_id);

    //Find client account details
    struct Account *client = client_find(client_id);
    if (client == NULL) return -1; //Invalid client, disconnect

    r_debug(VERBOSE, "Located client");
    r_debug_val(VERBOSE, "Auth", client->authentication);

    //Perform read call and store content in client buffer
    int received_bytes = (int) read(client_sock,
                                    client->client_message_buffer->buffer + client->client_message_buffer->pivot,
                                    network_get_available(client->client_message_buffer));

    //Update client struct
    client->client_message_buffer->length += received_bytes;
    client->client_message_buffer->pivot += received_bytes;

    //Debug
    r_debug_val(VERBOSE, "Received data", received_bytes);
    r_debug(VERBOSE, client->client_message_buffer->buffer);
    r_debug(VERBOSE, client->client_message_buffer->buffer + 4);

    if (received_bytes <= 0) return -1; //Disconnect
    else while (layer_read(client, client_id)); //Check for complete messages in buffer, if so: handle them

    return 0;
}