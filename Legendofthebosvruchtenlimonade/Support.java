package com.company;

public class Support {
    private int active_turn;
    private int active_support;

    Support() {
        this.active_turn = 0;
        this.active_support = 0;
    }

    void getInfo(){
        System.out.println("1\tLyanna\t\tHealer support\t\tHeals for 1.2*lvl + 22% of max HP\n" +
                "2\tJonathan\tDamage support\t\tDeals 1.6*lvl damage to the enemy\n" +
                "3\tKiranna\t\tHealer support\t\tHeals for 0.4*lvl + 8% of max HP and cleanses all debuffs/status effects\n" +
                "4\tVenza\t\tDebuffer support\tPoisons enemy. Poison deals 6% of max HP damage per turn\n" +
                "5\tRay\t\t\tBuffer support\t\tIncreases user's attack and agility by 30% for 2 turns\n" +
                "6\tGiaru\t\tBuffer support\t\tIncreases user's defense by 40% for 2 turns\n" +
                "7\tSarah\t\tDebuffer support\tReduces enemy's attack and agility by 30% for 2 turns\n" +
                "8\tKoto\t\tDebuffer support\tReduces enemy's defense by 40% for 2 turns.");
    }
    void doStuff(int supportId, Stats player, Stats enemy) {
        if (active_support == 0) this.active_support = supportId;

        switch (supportId) {
            case 1:
                //Lyanna	Healer support	Heals for 1.2*lvl + 22% of max HP
                if (active_turn == 0) doLyanna(player);
                break;

            case 2:
                //Jonathan 	Damage support	Deals 1.6*lvl damage to the enemy
                if (active_turn == 0) doJonathan(player, enemy);
                break;

            case 3:
                //Kiranna	Healer support	Heals for 0.4*lvl + 8% of max HP and cleanses all debuffs/status effects
                if (active_turn == 0) doKiranna(player);
                break;

            case 4:
                //Venza     Debuffer support	Poisons enemy. Poison deals 6% of max HP damage per turn
                doVenza(enemy);
                break;

            case 5:
                //Ray		Buffer support		Increases user's attack and agility by 30% for 2 turns.
                if (active_turn <= 1) doRay(player);
                break;

            case 6:
                //Giaru		Buffer support		Increases user's defense by 40% for 2 turns.
                if (active_turn <= 1) doGiaru(player);
                break;

            case 7:
                //Sarah		Debuffer support	Reduces enemy's attack and agility by 30% for 2 turns.
                if (active_turn <= 1) doSarah(enemy);
                break;

            case 8:
                //Koto		Debuffer support	Reduces enemy's defense by 40% for 2 turns.
                if (active_turn <= 1) doKoto(enemy);
                break;

            default:
                return;
        }
        active_turn++;
    }

    private void doLyanna(Stats player) {
        int playerLevel = player.getLVL();
        int playerHP = player.getDefaultHP();

        double heal = 1.2 * playerLevel + 0.22 * playerHP;

        player.setHP(player.getHP() + (int) Math.round(heal));
        this.active_turn = 1;
    }

    private void doJonathan(Stats player, Stats enemy) {
        int playerLevel = player.getLVL();

        double damage = 1.6 * playerLevel;

        enemy.setHP(enemy.getHP() - (int) Math.round(damage));
        this.active_turn = 1;
    }

    private void doKiranna(Stats player) {
        double heal = player.getLVL() * 0.4 + 0.8 * player.getDefaultHP();

        player.setHP(player.getHP() + (int) Math.round(heal));
    }

    private void doVenza(Stats enemy) {
        double poisonDamage = enemy.getDefaultHP() * 0.06;

        enemy.setHP(enemy.getHP() - (int) Math.round(poisonDamage));
    }

    private void doRay(Stats player) {
        double bonusAtk = 0.3 * player.getATK();
        player.setATK(player.getATK() + (int) Math.round(bonusAtk));

        double bonusAgi = 0.3 * player.getAGI();
        player.setAGI(player.getAGI() + (int) Math.round(bonusAgi));
    }

    private void doGiaru(Stats player) {
        double bonusDef = 0.4 * player.getDEF();

        player.setDEF(player.getDEF() + (int) Math.round(bonusDef));
    }

    private void doSarah(Stats enemy) {
        double attackDebuff = enemy.getATK() * 0.3;
        enemy.setATK(enemy.getATK() - (int) Math.round(attackDebuff));

        double agilityDebuff = enemy.getAGI() * 0.3;
        enemy.setAGI(enemy.getAGI() - (int) Math.round(agilityDebuff));
    }

    private void doKoto(Stats enemy) {
        double defenseDebuff = enemy.getDEF() * 0.4;

        enemy.setDEF(enemy.getDEF() - (int) Math.round(defenseDebuff));
    }
}