package com.company;

import java.util.ArrayList;

public class Unit {
    private Stats stats;
    private boolean inBattle;
    private String username;
    private Unit enemy;
    private String menuChoice;
    private int random;
    private int roundDamage;
    private int supp1;

    public int getSupp1() {
        return supp1;
    }

    public void setSupp1(int supp1) {
        this.supp1 = supp1;
    }

    public int getSupp2() {
        return supp2;
    }

    public void setSupp2(int supp2) {
        this.supp2 = supp2;
    }

    private int supp2;

    public int getTurnCount() {
        return turnCount;
    }

    public void setTurnCount(int turnCount) {
        this.turnCount = turnCount;
    }

    private int turnCount;

    public Unit(String username) {
        this.stats = new Stats();

        this.username = username;
        stats.setLVL(1);
        random = (int )(Math.random() * 8 + 16);
        stats.setDefaultHP(random);
        random = (int )(Math.random() * 6 + 7);
        stats.setDefaultATK(random);
        random = (int )(Math.random() * 6 + 7);
        stats.setDefaultDEF(random);
        random = (int )(Math.random() * 6 + 7);
        stats.setDefaultAGI(random);

        stats.setAllStats();
        inBattle = false;
        stats.setExpReq(100);
        stats.setExp(0);
    }

    public void startFight(Unit enemy) {
        stats.setAllStats();
        inBattle = true;
        this.enemy = enemy;
        if (!enemy.inBattle) {
            enemy.startFight(this);
            System.out.println("Initiating battle");
        }
    }

    public void endFight() {
        inBattle = false;
        stats.setAllStats();
    }

    public void giveExp(int xp) {

        this.getStats().addExp(xp);
    }

    public boolean checkDeath() {
        return this.stats.getHP() <= 0;
    }

    public void takeDamage() {
        //This functions returns your new HP
        double total_damg = (Math.pow(enemy.getStats().getLVL(), 0.48) * (enemy.getStats().getATK()/(1.2*this.getStats().getDEF())) * Math.pow(enemy.getStats().getAGI(), 0.25) * 2.5 + Math.pow(this.getStats().getLVL(), 0.40));
        total_damg = Math.round(total_damg);
        roundDamage = (int) total_damg;
        this.getStats().setHP(this.getStats().getHP() - roundDamage);

        if (this.getStats().getHP() <= 0) this.getStats().setHP(0);
        System.out.println(this.username + " received " + roundDamage + " damage!");
        System.out.println(this.username + " has " + this.getStats().getHP() + " HP left!");
    }

    public String getUsername() {
        return username;
    }

    public Stats getStats() {
        return stats;
    }

    //public Supports getSupports() { return supports[] }

    public void setStats(Stats stats) {
        this.stats = stats;
    }


    public String getMenuChoice() {
        return menuChoice;
    }

    public void setMenuChoice(String menuChoice) {
        this.menuChoice = menuChoice;
    }

}
