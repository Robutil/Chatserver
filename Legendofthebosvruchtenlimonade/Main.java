package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Support support = new Support();
        System.out.print("Enter player name: ");
        String user_input = input.nextLine();

        Unit player = new Unit(user_input);
        while (true) {
            System.out.println("Choose your action: ");
            System.out.println("1 Fight monster         2 Fight boss        3 Choose supports       4 Show stats           5 Exit game");
            player.setMenuChoice(input.nextLine());
            if (player.getMenuChoice().equals("Fight monster") ^ player.getMenuChoice().equals("1")) {
                Unit monster = new Unit("Robbin");
                monster.getStats().generateStats(player.getStats().getLVL());
                player.startFight(monster);
                //Following souts are for pure debugging purposes and will not take place in the actual game.
                System.out.println(player.getStats().getLVL());
                System.out.println(player.getStats().getHP());
                System.out.println(player.getStats().getATK());
                System.out.println(player.getStats().getDEF());
                System.out.println(player.getStats().getAGI());
                System.out.println(monster.getStats().getLVL());
                System.out.println(monster.getStats().getHP());
                System.out.println(monster.getStats().getATK());
                System.out.println(monster.getStats().getDEF());
                System.out.println(monster.getStats().getAGI());
                System.out.println(player.getStats().getATK() / (1.2 * monster.getStats().getDEF()));
                //Start of the battle loop
                while (true) {
                    System.out.println("Choose your command: ");
                    System.out.println("1 Attack          2 Call first support          3 Call second support            4 Run");
                    player.setMenuChoice(input.nextLine());
                    if (player.getMenuChoice().equals("Attack") ^ player.getMenuChoice().equals("1")) {
                        player.setTurnCount(player.getTurnCount() + 1);
                        System.out.println(player.getUsername() + ": " + player.getStats().getHP() + "/" + player.getStats().getDefaultHP());
                        System.out.println(monster.getUsername() + ": " + monster.getStats().getHP() + "/" + monster.getStats().getDefaultHP());
                        if (player.getStats().getAGI() >= monster.getStats().getAGI()) {
                            monster.takeDamage();
                            if (monster.checkDeath()) {
                                player.endFight();
                                player.giveExp(100);
                                player.getStats().setAllStats();
                                break;
                            }
                            player.takeDamage();
                            if (player.checkDeath()) {
                                System.out.println("You have died.");
                                player.endFight();
                                player.getStats().setAllStats();
                                break;
                            }
                        }
                        if (monster.getStats().getAGI() > player.getStats().getAGI()) {
                            player.takeDamage();
                            if (player.checkDeath()) {
                                System.out.println("You have died.");
                                player.endFight();
                                player.getStats().setAllStats();
                                break;
                            }
                            monster.takeDamage();
                            if (monster.checkDeath()) {
                                player.endFight();
                                player.giveExp(100);
                                player.getStats().setAllStats();
                                break;
                            }
                        }
                    }
                    if (player.getMenuChoice().equals("Call first support") ^ player.getMenuChoice().equals("2")) {
                        if (player.getStats().getAGI() >= monster.getStats().getAGI()) {

                            if (monster.checkDeath()) {
                                player.endFight();
                                player.giveExp(100);
                                player.getStats().setAllStats();
                                break;
                            }
                            player.takeDamage();
                            if (player.checkDeath()) {
                                System.out.println("You have died.");
                                player.endFight();
                                player.getStats().setAllStats();
                                break;
                            }
                        }
                        if (monster.getStats().getAGI() > player.getStats().getAGI()) {
                            player.takeDamage();
                            if (player.checkDeath()) {
                                System.out.println("You have died.");
                                player.endFight();
                                player.getStats().setAllStats();
                                break;
                            }
                            monster.takeDamage();
                            if (monster.checkDeath()) {
                                player.endFight();
                                player.giveExp(100);
                                player.getStats().setAllStats();
                                break;
                            }
                        }
                    }
                }
            }
            if (player.getMenuChoice().equals("Fight boss") ^ player.getMenuChoice().equals("2")) {
                System.out.println("This function hasn't been implemented yet. Please choose another one.");
            }

            if (player.getMenuChoice().equals("Choose supports") ^ player.getMenuChoice().equals("3")) {
                support.getInfo();
                System.out.print("Choose your first support: ");
                //todo check for integer
                player.setSupp1(Integer.parseInt(input.nextLine()));
                System.out.print("Choose your second support: ");
                String supp2 = input.nextLine();
                while (Integer.parseInt(supp2) == player.getSupp1()) {
                    System.out.print("You cannot choose the same support twice you dumbfuck.\n" +
                    "Choose your second support: ");
                    supp2 = input.nextLine();
                }
                player.setSupp2(Integer.parseInt(supp2));
            }

            if (player.getMenuChoice().equals("Show stats") ^ player.getMenuChoice().equals("4")) {
                while (true) {
                    System.out.println("Level: " + player.getStats().getLVL());
                    System.out.println("Hitpoints: " + player.getStats().getHP());
                    System.out.println("Attack: " + player.getStats().getATK());
                    System.out.println("Defense: " + player.getStats().getDEF());
                    System.out.println("Agility: " + player.getStats().getAGI());
                    System.out.println("Press Enter to continue...");
                    try {
                        System.in.read();
                    } catch (Exception e) {
                    }
                    break;
                }
            }

            //Ends the game completely
            if (player.getMenuChoice().equals("Exit game") ^ player.getMenuChoice().equals("5")) break;
        }
    }
}
