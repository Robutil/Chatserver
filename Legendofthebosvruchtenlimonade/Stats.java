package com.company;

public class Stats {
    private int HP;
    private int defaultHP;
    private int ATK;
    private int defaultATK;
    private int DEF;
    private int defaultDEF;
    private int AGI;
    private int defaultAGI;
    private int LVL;
    private int expReq;
    private int exp;
    private int random;


    public void setAllStats() {
        HP = defaultHP;
        ATK = defaultATK;
        DEF = defaultDEF;
        AGI = defaultAGI;
    }

    public void addExp(int xp){
        exp =+ xp;
        if(exp >= expReq){
            ++LVL;
            exp = 0;
            expReq = 100;
            System.out.println("Congratulations! You are now level " + LVL);
            lvlUp();
        }
    }

    private void lvlUp() {
        random = (int )(Math.random() * 3 + 4);
        this.setDefaultHP(this.getDefaultHP() + random);
        System.out.println("HP + " + random);
        random = (int )(Math.random() * 3 + 1);
        this.setDefaultATK(this.getDefaultATK() + random);
        System.out.println("Attack + " + random);
        random = (int )(Math.random() * 3 + 1);
        this.setDefaultDEF(this.getDefaultDEF() + random);
        System.out.println("Defense + " + random);
        random = (int )(Math.random() * 3 + 1);
        this.setDefaultAGI(this.getDefaultAGI() + random);
        System.out.println("Agility + " + random);
    }

    public void generateStats(int levelP) {
        for(int x=1; x<levelP; x++){
            this.setLVL(this.getLVL()+1);
            random = (int )(Math.random() * 3 + 4);
            this.setDefaultHP(this.getDefaultHP() + random);
            random = (int )(Math.random() * 3 + 1);
            this.setDefaultATK(this.getDefaultATK() + random);
            random = (int )(Math.random() * 3 + 1);
            this.setDefaultDEF(this.getDefaultDEF() + random);
            random = (int )(Math.random() * 3 + 1);
            this.setDefaultAGI(this.getDefaultAGI() + random);
        }
    }

    public int getAGI() {
        return AGI;
    }

    public void setAGI(int AGI) {
        this.AGI = AGI;
    }

    public int getATK() {
        return ATK;
    }

    public void setATK(int ATK) {
        this.ATK = ATK;
    }

    public int getDEF() {
        return DEF;
    }

    public void setDEF(int DEF) {
        this.DEF = DEF;
    }

    public int getDefaultAGI() {
        return defaultAGI;
    }

    public void setDefaultAGI(int defaultAGI) {
        this.defaultAGI = defaultAGI;
    }

    public int getDefaultATK() {
        return defaultATK;
    }

    public void setDefaultATK(int defaultATK) {
        this.defaultATK = defaultATK;
    }

    public int getDefaultDEF() {
        return defaultDEF;
    }

    public void setDefaultDEF(int defaultDEF) {
        this.defaultDEF = defaultDEF;
    }

    public int getDefaultHP() {
        return defaultHP;
    }

    public void setDefaultHP(int defaultHP) {
        this.defaultHP = defaultHP;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getExpReq() {
        return expReq;
    }

    public void setExpReq(int expReq) {
        this.expReq = expReq;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getLVL() {
        return LVL;
    }

    public void setLVL(int LVL) {
        this.LVL = LVL;
    }

}
