public class Handler implements Module {
    private Connect connect; //!
    private FourTwenty fourTwenty;

    @Override
    public void init(Connect connect) {
        this.connect = connect;
        this.fourTwenty = new FourTwenty();
        connect.send_message(fourTwenty.show_field());
    }

    @Override
    public void receive_handler(String message) {
        System.out.println("Got message: " + message);
        if (message.contains("new")) {
            this.fourTwenty = new FourTwenty();
            connect.send_message(fourTwenty.show_field());
        } else {
            String answer = fourTwenty.guess(message);
            if (answer.contains("died")) {
                connect.send_message(answer);
                fourTwenty = new FourTwenty();
            } else {
                connect.send_message(answer);
            }
        }
    }

    @Override
    public void run() {
        //Does nothing
    }
}
