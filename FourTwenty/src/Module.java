/*
* Init is called once at the start and a reference to Connect is provided.
* Use this reference to send messages back to the server. The receive-
* handler is called by Connect when the Module is being called from the server.
* */

public interface Module extends Runnable {

    //Use this to store the connect reference locally
    public void init(Connect connect);

    //Connect calls this when data is available, create
    public void receive_handler(String message);

    /*
    * Module implements the Runnable class to allow for procedures that require
    * a time-frame to operate, without the need to create another dedicated class.
    * Connect will only create this thread if it is requested, this can be done
    * in the initialisation as well as on a trigger by receive using the start_thread
    * method. If variables are required for the thread, store them locally.
    * */
}
