public class FourTwenty {

    private String field;
    private boolean is_white_turn;
    private int number_of_white_stones;

    //TODO: Second field array to ensure valid moves

    public FourTwenty() {
        field = "4)[z]-[ ]-[ ]-[ ]-[z]\n" +
                "   | \\ | / | \\ | / |\n" +
                "3)[ ]-[ ]-[ ]-[ ]-[ ]\n" +
                "   | / | \\ | / | \\ |\n" +
                "2)[ ]-[ ]-[ ]-[ ]-[ ]\n" +
                "   | \\ | / | \\ | / |\n" +
                "1)[ ]-[ ]-[ ]-[ ]-[ ]\n" +
                "   | / | \\ | / | \\ |\n" +
                "0)[z]-[ ]-[ ]-[ ]-[z]\n" +
                "   0)  1)  2)  3)  4)";
        is_white_turn = false;
        number_of_white_stones = 0;
    }

    public String show_field() {
        if (is_white_turn) System.out.println("Is white turn");
        return field;
    }

    public String guess(String guess) {
        String[] guess_cor = guess.split("[\\. ]");
        int x1, y1, x2 = -1, y2 = -1;
        try {
            x1 = Integer.parseInt(guess_cor[0]);
            y1 = Integer.parseInt(guess_cor[1]);
            if (guess_cor.length > 2) {
                x2 = Integer.parseInt(guess_cor[2]);
                y2 = Integer.parseInt(guess_cor[3]);
            }
        } catch (Exception ex) {
            return "Unknown coordinate";
        }

        System.out.println("Current values: x1: " + x1 + ", y1: " + y1 + ", x2: " + x2 + ", y2: " + y2);

        if (x1 == x2 && y1 == y2) return "Illegal move";

        //Optimize these if-statements, at least 1 is redundant
        if (number_of_white_stones < 20 && is_white_turn && (x2 != -1 || y2 != -1))
            return "White requires a single coordinate";
        else if (number_of_white_stones >= 20 && is_white_turn && (x2 == -1 || y2 == -1))
            return "White requires two coordinates";

        int x_counter = 0, y_counter = 4;
        for (int i = 0; i < field.length(); i++) {
            System.out.print(field.charAt(i) + " ");
            if (field.charAt(i) == '[') {
                //Found a place
                if (x_counter == x1 && y_counter == y1) {
                    System.out.println("First move");
                    if (field.charAt(i + 1) == ' ' && is_white_turn) change_char_at_index(i + 1, 'w');
                    else if (field.charAt(i + 1) == 'z' && !is_white_turn) change_char_at_index(i + 1, ' ');
                    else return "Illegal move";
                } else if (x_counter == x2 && y_counter == y2) {
                    System.out.println("Second move " + x_counter + ", " + y_counter);
                    if (field.charAt(i + 1) == ' ') {
                        if (is_white_turn) change_char_at_index(i + 1, 'w');
                        else change_char_at_index(i + 1, 'z');
                    } else return "Illegal move";
                }
                System.out.print("("+ x_counter + "," + y_counter + ")");
                x_counter++;
                if (x_counter > 4) {
                    if (y_counter == 0) break;
                    x_counter = 0;
                    y_counter--;
                }
            }
        }
        System.out.println();
        is_white_turn = !is_white_turn;
        return show_field();
    }

    /*
    private void conditional_change_coordinate(int x, int y, char expect) {
        int x_counter = 0, y_counter = 4;
        for (int i = 0; i < field.length(); i++) {
            if (field.charAt(i) == '[') {
                //Found a place
                if (x_counter == x && y_counter == y) {
                    //Found right place
                    if (is_white_turn) {
                        if (number_of_white_stones >= 20) {
                            //Find other coordinate
                            for (int j = 0; j < field.length(); j++) {
                                //Set second stone

                            }

                        }

                        //Either set a stone or delete first
                        if (field.charAt(i + 1) == ' ') change_char_at_index(i + 1, 'w');
                        else change_char_at_index(i + 1, ' ');
                        break; //Finished
                    } else {

                    }
                }
                x_counter++;
                if (x_counter > 4) {
                    if (y_counter == 0) break;
                    x_counter = 0;
                    y_counter--;
                }
            }
        }
    }
    */

    private void change_char_at_index(int index, char replace) {
        char[] switcher = field.toCharArray();
        switcher[index] = replace;
        field = String.valueOf(switcher);
    }
}
