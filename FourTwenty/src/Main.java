import java.io.IOException;

public class Main {

    public static void main(String[] args) throws InterruptedException, IOException {

        //test

        FourTwenty paulos = new FourTwenty();
        String temp = paulos.show_field();
        System.out.println(temp);
        System.out.println(paulos.guess("0.0 0.1"));
        System.out.println(paulos.guess("0.0"));
        System.exit(0);

        /* === Alter this ===*/

        Module module = new Handler(); //Use your module here
        Connect connect = new Connect(module, "Minesweeper", "ms"); //Preferred handle and abbreviation

        /*
        * If the abbreviation is an empty String, "" or null, the module
        * is called with the entire message, including the format.
        * */

        /* === Predefined === */

        connect.toServer();
        Thread receive = new Thread(connect);
        receive.start();
        receive.join(); //Will hang until bot receives kill command or server dies
    }
}
