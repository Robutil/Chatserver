import java.util.Scanner;

public class Loader {

    private static int[] parseInput(String input) throws NumberFormatException {
        String[] split = input.split(" ");
        int numb[] = new int[split.length];

        for (int i = 0; i < split.length; i++) {
            numb[i] = Integer.parseInt(split[i]);
        }

        return numb;
    }

    public static void main(String[] args) {

        //Some variables
        int size = 0, mines = 0, wins = 0, losses = 0;
        int playcount = 0, totalmines_cleared = 0;

        //Parse starting parameters
        try {
            if (args.length > 0) {
                size = parseInput(args[0])[0];
                if (args.length > 1) {
                    mines = parseInput(args[1])[0];
                }
            }
        } catch (NumberFormatException ex) {
            System.out.println("Usage: \n\t[size] \t\t-> Size of the minefield \n\t[number] \t-> Amount of mines in the field");
            return;
        }

        //Create instance of a scanner
        Scanner input = new Scanner(System.in);

        //Minesweeper object, game logic
        MineSweeper ms;

        //Create instance of Minesweeper
        if (size != 0 && mines != 0) ms = new MineSweeper(size, mines);
        else if (size != 0) ms = new MineSweeper(size, mines);
        else ms = new MineSweeper();

        //Set variables for future games
        if (size == 0) size = 6; //Default
        if (mines == 0) mines = 6; //Default

        //Inform users of playing field
        System.out.println("Size: " + size + " Mines: " + mines);

        while (true) {
            //Show field
            System.out.println(ms.show_field());



            //Get user guess
            System.out.print("New guess: ");
            String userInput = input.nextLine();

            if (userInput.equals("Exit") || userInput.equals("exit")) break;

            ms.guess(userInput);

            if (ms.getDone()) {
                playcount++;
                totalmines_cleared += ms.getCounter();
                if (ms.getWin()) {
                    wins++;
                } else {
                    System.out.println("You died!!!");
                    System.out.println(ms.show_field_debug());
                    losses++;
                }
                System.out.println("Statistics: \n\t" + "Play count: " + playcount
                        + "\n\tWins: " + wins
                        + "\n\t Losses: " + losses
                        + "\n\tTotal mines cleared: " + totalmines_cleared + "\n");

                System.out.print("New game? (y/n)");
                if (input.nextLine().equals("y")) {
                    System.out.print("New parameters? (n/param)");
                    String newgame = input.nextLine();
                    if (!newgame.equals("n")) {
                        try {
                            int newparam[] = parseInput(newgame);
                            if (newparam.length > 0) {
                                size = newparam[0];
                                if (newparam.length > 1) {
                                    mines = newparam[1];
                                }
                            }
                        } catch (NumberFormatException ex) {
                            System.out.println("Usage: \n\t[size] \t\t-> Size of the minefield \n\t[number]" +
                                    " \t-> Amount of mines in the field\n Shutting down.");
                            return;
                        }
                    }
                    ms = new MineSweeper(size, mines);
                } else {
                    break;
                }
            }
        }
        System.out.println("Thanks for playing!");
    }
}

