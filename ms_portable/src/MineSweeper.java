//TODO: format when high rows

public class MineSweeper {
    private int def_x = 6;
    private int def_y = 6;
    private int mines = 6;
    private int counter = 0;
    private boolean done = false;
    private boolean win = false;
    private int[][] field;
    private char[][] c_field;

    public MineSweeper() {
        init_field();
    }

    public MineSweeper(int size) {
        this.mines = size;
        this.def_x = size;
        this.def_y = size;
        init_field();
    }

    public MineSweeper(int size, int mines) {
        this.mines = mines;
        this.def_x = size;
        this.def_y = size;
        init_field();
    }

    public String guess(String guess) {
        String[] guess_cor = guess.split("\\.");
        int x, y;
        try {
            x = Integer.parseInt(guess_cor[0]);
            y = Integer.parseInt(guess_cor[1]);
        } catch (Exception ex) {
            return "Unknown coordinate";
        }
        // System.out.println(x + " --- " + y);
        y = def_y - y - 1;
        // System.out.println(x + " --- " + y);
        if (x > def_x - 1 || y > def_y - 1 || x < 0 || y < 0) {
            return "Out of bounds";
        }


        if (!(c_field[y][x] == 'x' || c_field[y][x] == 'b')) {
            return "Square already taken";
        }
        if (guess_cor.length > 2) {
            if (guess_cor[2].equals("b")) {
                if (c_field[y][x] == 'b') {
                    return "Square is already flagged";
                }
                c_field[y][x] = 'b';
            } else if (guess_cor[2].equals("x")) {
                if (c_field[y][x] == 'x') {
                    return "Square has no pending flags";
                }
                c_field[y][x] = 'x';
            } else {
                return "Unknown token";
            }
            return "Polled square";
        }
        counter++;
        if (field[y][x] == -1) {
            c_field[y][x] = 'o';
            done = true;
            return "You died!!!";
        } else {
            c_field[y][x] = Character.forDigit(field[y][x], 10);
            if (field[y][x] == 0) {
                counter += clear_row(y, x);
            }
            show_field();
            System.out.print("Current counter: " + counter);
            if (counter == def_x * def_y - mines) {
                System.out.println(". You won!!!");
                System.out.println(show_field());
                done = true;
                win = true;
                return "You won!!!";
            }
            return show_field();
        }
    }

    private int clear_row(int x, int y) {
        int checkx = x - 1;
        int checky = y - 1;
        int count = 0;
        for (int k = checkx; k < x + 2; k++) {
            for (int l = checky; l < y + 2; l++) {
                if (k >= 0 && k < def_x && l >= 0 && l < def_y) {
                    if (c_field[k][l] == 'x') {
                        count++;
                        c_field[k][l] = Character.forDigit(field[k][l], 10);

                        if (field[k][l] == 0) count += clear_row(k, l);
                    }
                }
            }
        }
        return count;
    }

    private void init_field() {
        field = new int[def_x][def_y];
        //Insert mines
        for (int i = 0; i < mines; i++) {
            int minx = (int) (Math.random() * def_x);
            int miny = (int) (Math.random() * def_y);

            if (field[minx][miny] == -1) {
                i--;
            } else {
                field[minx][miny] = -1;
            }
        }

        //Calculate non-mines numbers
        for (int i = 0; i < def_x; i++) {
            for (int j = 0; j < def_y; j++) {
                int checkx = i - 1;
                int checky = j - 1;
                int count = 0;
                for (int k = checkx; k < i + 2; k++) {
                    for (int l = checky; l < j + 2; l++) {
                        if (k >= 0 && k < def_x && l >= 0 && l < def_y) {
                            if (field[k][l] == -1) {
                                count++;
                            }
                        }
                    }
                }
                if (field[i][j] != -1 && field[i][j] == 0) field[i][j] = count;
            }
        }

        //User interface
        c_field = new char[def_x][def_y];
        for (int i = 0; i < def_x; i++) {
            for (int j = 0; j < def_y; j++) {
                c_field[i][j] = 'x';
            }
        }
    }

    public String show_field_debug() {
        String fullField = "\n";
        for (int i = 0; i < def_x; i++) {
            for (int j = 0; j < def_y; j++) {
                fullField = String.format(fullField + " [" + field[i][j] + "] ");
            }
            fullField = String.format(fullField + "\n");
        }
        return fullField;
    }

    public String show_field() {
        String fullField = "\n";
        for (int i = 0; i < def_x; i++) {
            fullField = String.format("%s (%d)", fullField, def_x - i - 1);
            for (int j = 0; j < def_y; j++) {
                fullField = String.format("%s[%c] ", fullField, c_field[i][j]);
            }
            fullField = String.format("%s\n", fullField);
        }
        fullField = String.format("%s    ", fullField);
        for (int i = 0; i < def_y; i++) {
            fullField = String.format("%s(%d) ", fullField, i);
        }
        return fullField;
    }

    public int getMines() {
        return mines;
    }

    public int getCounter() {
        return counter;
    }

    public boolean getDone() {
        return done;
    }

    public boolean getWin() {
        return win;
    }
}


