#ifndef LIST_H
#define LIST_H

#include "server.h"
#include "channel.h"

struct Data {
    int permission;
    uint16_t inc_port;
    char client_handle[HANDLE_SIZE];
    struct ArrayList *channel;
    char message_buffer[4096];
    int points;
};

void Data_init(struct Data *temp);

void add_to_channel(int id, int channel);

int linkedList_id_FB_token(int token);

int linkedList_socket_FB_id(int id);

struct Data *linkedList_Data_FB_id(int private_token);

struct Data *linkedList_Data_FB_token(int private_token);

void linkedList_remove_token(int temp_id);

void linkedList_remove_id(int temp_id);

int linkedList_add(struct Data temp, int sock);

int linkedList_init();

#endif

