#ifndef BUFFER_SIZE
#define BUFFER_SIZE 1024
#endif

#ifndef PROTOCOL_H
#define PROTOCOL_H

struct r_Packet {
    char *raw;
    char *format;
    struct r_Packet *next;
};

int unwrap_number(char *req, int start, char split_token);

void packet_init(struct r_Packet *req);

void create_packet(struct r_Packet *req, char *message);

void clear_packet(struct r_Packet *req);

void *unwrap_packet(struct r_Packet *req);

#endif
