#ifndef ARRAYLIST_H
#define ARRAYLIST_H

struct ArrayList {
    size_t length;
    int *key;
};

struct ArrayList *arrayList_init();
void arrayList_add(struct ArrayList *list, int value);
int arrayList_remove(struct ArrayList *list, int value);

#endif
