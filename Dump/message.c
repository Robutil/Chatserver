#include <string.h>
#include <stdio.h>
#include <sys/unistd.h>
#include <stdlib.h>
#include <ctype.h>

#include "message.h"
#include "../src/Utilities/robutil.h"
#include "server.h"
#include "list.h"
#include "../Server/Network/format.h"
#include "../src/System/system.h"
#include "protocol.h"

void channel_handler(struct message_wrapper req) {
    r_log("Received a channel message");
    struct ArrayList *recp = linkedList_Data_FB_id(req.recv_id)->channel;
    if (recp->length == 0) return;
    int i;
    for (i = 0; i < recp->length; ++i) {
        if (recp->key[i] != req.req_token) write(recp->key[i], req.buffer, strlen(req.buffer));
    }
}

void announce_channel(char *message, int skip_token, int channel) {
    struct message_wrapper req;
    req.buffer = malloc(BUFFER_SIZE);
    req.recv_id = channel; //unsafe
    req.req_token = skip_token;
    memcpy(req.buffer, message, strlen(message));
    channel_handler(req);
}

int sys_command(struct message_wrapper *req) {
    if (req->buffer[0] != '/') return 0;
    char command = req->buffer[1];
    if (isdigit(command)) command = 'd';

    printf("Received a system command: ");
    fflush(stdout);

    switch (command) {
        case 'd':
            r_log("Targeted message");
            sys_targeted_message(req);
            break;
        case 's':
            r_log("Show clients.");
            sys_show_clients(req);
            break;
        case 'n':
            r_log("New channel.");
            sys_new_channel(req);
            break;
        case 'j':
            r_log("Join channel.");
            sys_join_channel(req);
            break;
        case 'l':
            r_log("Leave channel.");
            sys_leave_channel(req);
            break;
        case 'k':
            r_log("End channel.");
            sys_end_channel(req);
            break;
        default:
            r_log("Unknown.");
    }
    return 1;
}

int layer_check_buffer(int client_sock) {
    struct message_wrapper req;
    req.buffer = malloc(BUFFER_SIZE);
    req.req_token = client_sock;
    req.req_id = linkedList_id_FB_token(client_sock);
    bzero(req.buffer, BUFFER_SIZE);

    int received_bytes;
    received_bytes = read(client_sock, req.buffer, BUFFER_SIZE);

    if (received_bytes < 0) return -1;
    else if (received_bytes == 0) return -1; //end of file
    else {
        struct Data *data_pointer = linkedList_Data_FB_token(client_sock);
        strncat(data_pointer->message_buffer, req.buffer, (size_t) received_bytes);
        //Handle message...

        struct r_Packet network_temp;
        network_temp.format = malloc(strlen(req.buffer));
        strncpy(network_temp.format, req.buffer, strlen(req.buffer));
        unwrap_packet(&network_temp);

        printf("Server: got message: %s", req.buffer);

        if (sys_command(&req)) return 0;

        if (unwrap_id(req.buffer, ':') == -1) {
            write(req.req_token, "Unknown channel\n", strlen("Unknown channel\n"));
            return 1;
        } else {
            format_message(&req);
            if (req.recv_token == 0) {
                channel_handler(req);
            } else {
                write(req.recv_token, req.buffer, strlen(req.buffer));
            }
        }
    }
    return 0;
}