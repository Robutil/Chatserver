#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "server.h"
#include "list.h"
#include "../src/Utilities/robutil.h"
#include "channel.h"

#define PARENT 1
#define ACTUAL 0

//TODO: clean this up, obselete functions

struct Node {
    struct Node *next_node;
    struct Data data;
    int local_id;
    //if sock == 0 then it's a channel
    int sock;
};

static int id_counter = 0;
static struct Node *root;

/*=== Linked List===*/

void Data_init(struct Data *temp) {
    bzero(temp->client_handle, HANDLE_SIZE);
    temp->inc_port = 0;
    temp->channel = NULL;
    bzero(temp->message_buffer, 4096);
    temp->permission = 0;
    temp->points = 0;
}

static struct Node *linkedList_Node_FB_id(int temp_id, int ret_parent) {
    //Returns NULL if client doesn't exist, flag returns parent
    struct Node *cycle;
    cycle = root;
    while (cycle->local_id != temp_id) {
        //printf("Handle: %s. Sock: %d. Token: %d. Id: %d\n", cycle->data.client_handle, cycle->sock,
        //     cycle->data.inc_port, cycle->local_id);
        if (cycle->next_node == 0) return NULL;
        if (ret_parent && cycle->next_node->local_id == temp_id) return cycle;
        cycle = cycle->next_node;
    }
    return cycle;
}

static struct Node *linkedList_Node_FB_socket(int sock, int ret_parent) {
    //Returns NULL if client doesn't exist, flag returns parent
    struct Node *cycle;
    cycle = root;
    while (cycle->sock != sock) {
        if (cycle->next_node == 0) return NULL;
        if (ret_parent && cycle->next_node->sock == sock) return cycle;
        cycle = cycle->next_node;
    }
    return cycle;
}

static int new_node(struct Node **node) {
    //Returns an empty, but usable Node
    *node = malloc(sizeof(struct Node));
    (*node)->next_node = 0;
    return (*node)->local_id = 0;
}

static int insert_Node(struct Node *node, struct Data info, int sock) {
    //Adds a client's info to a (new) node, returns local id
    if (node->local_id == 0) {
        node->local_id = ++id_counter;
        node->sock = sock;
        node->data = info;
        node->next_node = 0;
        node->data.channel = arrayList_init();
        return node->local_id;
    } else {
        if (node->next_node == 0) r_error_ld(new_node(&node->next_node), "Node");
        return insert_Node(node->next_node, info, sock);
    }
}

void add_to_channel(int id, int channel) {
    struct Node *temp = linkedList_Node_FB_id(channel, ACTUAL);
    arrayList_add(temp->data.channel, id);
    temp = linkedList_Node_FB_socket(id, ACTUAL);
    arrayList_add(temp->data.channel, channel);
}

int linkedList_id_FB_token(int token) {
    return linkedList_Node_FB_socket(token, ACTUAL)->local_id;
}

int linkedList_socket_FB_id(int id) {
    //Returns -1 if not found
    struct Node *temp = linkedList_Node_FB_id(id, ACTUAL);
    if (temp == NULL) return -1;
    else return temp->sock;
}

struct Data *linkedList_Data_FB_id(int id) {
    //Returns NULL if client doesn't exist
    struct Node *temp = linkedList_Node_FB_id(id, ACTUAL);
    if (temp == NULL) return NULL;
    else return &temp->data;
}

struct Data *linkedList_Data_FB_token(int private_token) {
    //Returns NULL if client doesn't exist
    return &linkedList_Node_FB_socket(private_token, ACTUAL)->data;
}

//TODO: this
int linkedList_remove_channel(int temp_id) {
    struct Node *cycle, *kill_node, *parent_node = linkedList_Node_FB_id(temp_id, PARENT);
    if (parent_node == NULL) return -1;
    kill_node = parent_node->next_node;
    parent_node->next_node = kill_node->next_node;
    int i;
    for (i = 0; i < kill_node->data.channel->length; ++i) {
        cycle = linkedList_Node_FB_socket(kill_node->data.channel->key[i], ACTUAL);
        arrayList_remove(cycle->data.channel, temp_id);
    }
    free(kill_node);
    return 0;
}

void linkedList_remove_token(int temp_id) {
    struct Node *kill_node, *parent_node = linkedList_Node_FB_socket(temp_id, PARENT);
    if (parent_node == NULL) r_error("No such client");
    kill_node = parent_node->next_node;
    parent_node->next_node = kill_node->next_node;
    int i;
    struct ArrayList *temp;
    for (i = 0; i < kill_node->data.channel->length; ++i) {
        temp = linkedList_Data_FB_id(kill_node->data.channel->key[i])->channel;
        arrayList_remove(temp, temp_id);
    }
    free(kill_node);
}

void linkedList_remove_id(int temp_id) {
    //For channels only
    struct Node *kill_node, *parent_node = linkedList_Node_FB_id(temp_id, PARENT);
    kill_node = parent_node->next_node;
    parent_node->next_node = kill_node->next_node;
    free(kill_node);
}

int linkedList_add(struct Data temp, int sock) {
    //Returns id
    return insert_Node(root, temp, sock);
}

int linkedList_init() {
    //Creates a starting point in memory, empty info but mandatory
    return new_node(&root);
}
