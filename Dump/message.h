#ifndef LAYER_H
#define LAYER_H

struct message_wrapper {
    int recv_id;
    int recv_token;
    char *buffer;
    int req_id;
    int req_token;
};

int layer_check_buffer(int client_sock);

void channel_handler(struct message_wrapper req);

void announce_channel(char *message, int skip_token, int channel);

#endif
