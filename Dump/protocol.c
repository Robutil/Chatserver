#include <stdlib.h>
#include <strings.h>
#include <stdio.h>
#include <string.h>

#include "protocol.h"
#include "../Server/Utilities/robutil.h"

#define SOM '<'
#define EOM '>'

int unwrap_number(char *req, int start, char split_token) {
    int i, temp_id = 0, flag = 1;
    for (i = start; i < strlen(req); i++) {
        temp_id += req[i] - '0';
        if (req[i + 1] == split_token) {
            flag = 0;
            break;
        }
        temp_id *= 10;
    }
    if (flag) return -1;
    else return temp_id;
}

void packet_init(struct r_Packet *temp) {
    temp->raw = NULL;
    temp->format = NULL;
    temp->next = NULL;
}

void clear_packet(struct r_Packet *req) {
    //Keep in mind that this doesn't clear previous pointers and can't clear locals
    struct r_Packet *kill, *cycle = req;

    while (cycle->next != NULL) {
        //TODO: this shouldn't work. But it does...
        //TODO:UPDATE: I fixed it, it doesn't work anymore

        kill = cycle;
        r_log(kill->raw);
        cycle = cycle->next; //Possible break here

        if (kill->format != NULL) free(kill->format);
        if (kill->raw != NULL) free(kill->raw);
        free(kill);
    }
}

void create_packet(struct r_Packet *req, char *message) {
    char *format = malloc(BUFFER_SIZE);
    bzero(format, BUFFER_SIZE);

    sprintf(format, "%c%04d:%s%c", SOM, (int) strlen(message), message, EOM);

    req->format = format;
    req->raw = malloc(strlen(message));

    bzero(req->raw, strlen(message));
    strncpy(req->raw, message, strlen(message));
    r_log(req->format);
}

void *unwrap_packet(struct r_Packet *req) {
    if (req->format == NULL) return NULL;
    char *full_message = malloc(strlen(req->format));
    strncpy(full_message, req->format, strlen(req->format));
    struct r_Packet *cycle = req;

    int i, first_cycle = 1;
    size_t helper = 0;
    for (i = 0; i < strlen(full_message); ++i) {
        if (full_message[i] == SOM) {
            helper = (size_t) unwrap_number(req->format, (i + 1), ':');
            if (req->format[helper + 6 + i] != EOM) {
                r_log("Message does not abide by specified format");
                break; //TODO: potentially handle other packets
            } else {
                if (!first_cycle) {
                    cycle->next = malloc(sizeof(struct r_Packet));
                    packet_init(cycle->next);
                    cycle = cycle->next;
                } else {
                    first_cycle = 0;
                    cycle->next = NULL;
                }
                cycle->raw = malloc(helper);
                strncpy(cycle->raw, &full_message[i + 6], helper);

                i += helper + 6;
            }
        }
    }
    return req;
}

