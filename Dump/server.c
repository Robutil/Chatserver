#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "server.h"
#include "message.h"
#include "protocol.h"
#include "list.h"
#include "../Server/Utilities/robutil.h"
#include "../Server/Utilities/sleep.h"

//TODO: remove channels -> client side / server remove
//TODO: network protocol
//TODO: single message handshake -> DONE -> TODO: NO HANDSHAKE ON CONNECT!
//TODO: duplicate names
//TODO: moderators
//TODO: point system
//TODO: fix crash when unknown channel (0)

#define PORT_DEF "2021"

static int server_sock, cleanup = 0;

void sig_handler(int sig) {
    if (sig == SIGINT) {
        r_log("Closing sockets... ");
        cleanup = 1;
        close(server_sock);
        sleep_mil(500);
        exit(0);
    } else {
        r_error("SIG");
    }
}

void client_channel(int socket, int channel, int rejoin) {
    if (!rejoin) add_to_channel(socket, channel);
    char *buffer, *temp = linkedList_Data_FB_id(channel)->client_handle;
    buffer = malloc(BUFFER_SIZE);
    bzero(buffer, BUFFER_SIZE);

    struct r_Packet req;
    sprintf(buffer, "/c:%d:%s", channel, temp);

    create_packet(&req, buffer);
    r_log(req.format);
    write(socket, req.format, strlen(req.format));
    free(req.format);
    free(req.raw);
}

int client_handler(int socket, uint16_t port) {
    //TODO: no handshaking here
    //Variables
    struct Data list_client;
    Data_init(&list_client);

    char client_message[BUFFER_SIZE];
    bzero(client_message, BUFFER_SIZE);

    int temp = 0;

    //Handshake
    if (recv(socket, client_message, HANDLE_SIZE, 0) > 0) {
        //Get client preferred handle
        //TODO: no duplicate / system names. See prev TODO.

        struct r_Packet req;
        req.format = malloc(strlen(client_message));
        strncpy(req.format, client_message, strlen(client_message));
        unwrap_packet(&req);

        strncpy(list_client.client_handle, req.raw, strlen(req.raw));
        list_client.inc_port = port;

        clear_packet(req.next);

        //Assign id, for chat-use
        temp = linkedList_add(list_client, socket);
        sprintf(client_message, "%d", temp);
        create_packet(&req, client_message);
        write(socket, req.format, strlen(req.format));

        client_channel(socket, 1, 0);

        printf("%s joined global\n", list_client.client_handle);
    } else {
        r_error_soft("Empty client");
        return -1;
    }
    if (strlen(client_message) == 0) close(socket);

    r_log("Finished handler");
    return temp;
}

int main(int argc, char *argv[]) {
    int i;
    char port_fin[6];
    struct sockaddr_in client_add;
    struct Data server_user;

    struct addrinfo data, *server_info;;
    fd_set fd_active, fd_read;
    size_t size;

    //Get preferred port
    if (argc < 2) {
        printf("No port specified, using default: %s\n", PORT_DEF);
        strncpy(port_fin, PORT_DEF, strlen(PORT_DEF));
    } else {
        strncpy(port_fin, argv[1], strlen(argv[1]));
    }

    //Set socket flags
    memset(&data, 0, sizeof(data));
    data.ai_family = AF_INET;
    data.ai_socktype = SOCK_STREAM;
    data.ai_flags = AI_PASSIVE;

    if (getaddrinfo(NULL, port_fin, &data, &server_info) != 0) r_error("addrinfo");

    server_sock = socket(server_info->ai_family, server_info->ai_socktype, server_info->ai_protocol);

    if (server_sock < 0) r_error("socket");

    if (bind(server_sock, server_info->ai_addr, server_info->ai_addrlen) == -1) r_error("bind");

    if (signal(SIGINT, sig_handler) == SIG_ERR) r_error("SIG");

    if (linkedList_init() == -1) r_error("rootlist");

    Data_init(&server_user);
    strncpy(server_user.client_handle, "Global", 6);
    linkedList_add(server_user, 0);

    if (listen(server_sock, CLIENT_NUMB) < 0) r_error("listen");

    FD_ZERO(&fd_active);
    FD_SET(server_sock, &fd_active);

    while (!cleanup) {
        fd_read = fd_active;
        if (pselect(FD_SETSIZE, &fd_read, NULL, NULL, NULL, NULL) < 0) r_error("pselect");
        for (i = 0; i < FD_SETSIZE; i++) {
            if (FD_ISSET (i, &fd_read)) {
                if (i == server_sock) {
                    int new;

                    //Accept
                    size = sizeof(struct sockaddr_in);
                    new = accept(server_sock, (struct sockaddr *) &client_add, (socklen_t *) &size);
                    if (new < 0) r_error("connect");

                    //Log
                    fprintf(stderr, "Server: connect from host %s, port %d.\n",
                            inet_ntoa(client_add.sin_addr),
                            ntohs(client_add.sin_port));

                    //Allocate FD
                    FD_SET(new, &fd_active);

                    //Link client
                    client_handler(new, client_add.sin_port);
                    bzero(&client_add, sizeof(struct sockaddr_in));
                } else if (layer_check_buffer(i) < 0) {
                    printf("Removed %s\n", linkedList_Data_FB_token(i)->client_handle);
                    linkedList_remove_token(i);
                    close(i);
                    FD_CLR(i, &fd_active);
                }
            }
        }
    }
    close(server_sock);
    return 0;
}
