package chatserver;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws InterruptedException, IOException {

        /* === Alter this ===*/

        Module module = new Client(); //Use your module here
        Handler handler = new Handler(module, "SegFaultInc", ""); //Preferred handle and abbreviation

        /*
        * If the abbreviation is an empty String, "" or null, the module
        * is called with the entire message, including the format.
        * */

        /* === Predefined === */

        Thread receive = new Thread(handler);
        receive.start();
        receive.join(); //Will hang until program receives kill command or server dies
    }
}
