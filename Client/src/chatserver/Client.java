package chatserver;

import java.util.Scanner;

public class Client implements Module {
    private Handler handler; //!

    @Override
    public void init(Handler handler) {
        this.handler = handler;
        //if (!connect.start_thread()) throw new RuntimeException();
    }

    @Override
    public void receiveHandler(String message) {
        System.out.println("Got message: " + message);
    }

    @Override
    public void run() {
        Scanner input = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            String temp = input.next();
            if (temp.equals("exit")) {
                //connect.terminate();
            } else {
                System.out.println("Parsing " + temp);
                //connect.send_message(temp);
            }
        }
    }
}
