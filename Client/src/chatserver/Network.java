package chatserver;

/*
 * Defined in Client, on 8/12/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: -
 *
 * Summary: -
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import network.Packet;
import network.Protocol;

import java.io.IOException;
import java.nio.ByteBuffer;

public class Network implements Protocol {

    @Override
    public Packet readPacket(ByteBuffer inputStreamBuffer) {

        if (inputStreamBuffer.position() != 0) inputStreamBuffer.flip();

        int length = inputStreamBuffer.getInt();

        if (length == 0) System.out.println("Empty packet!");

        if (inputStreamBuffer.remaining() >= length) {
            char[] content = new char[length];
            for (int i = 0; i < length; i++) {
                content[i] = (char) inputStreamBuffer.get();
            }

            System.out.println(new String(content));
            //packet.
        }

        Packet packet = new ChatserverPacket();
        //packet.unlock(buffer);

        return packet;
    }

    @Override
    public void addPacket(ByteBuffer outputStreamBuffer, Packet packet) {

//        outputStreamBuffer.clear();
//        outputStreamBuffer.putInt(packet.id());
//
//        packet.lock(outputStreamBuffer);
//
//        int length = outputStreamBuffer.position();
//        buffer.putInt(length);
//
//        outputStreamBuffer.flip();
//        buffer.put(outputStreamBuffer);

        return;
    }
}
