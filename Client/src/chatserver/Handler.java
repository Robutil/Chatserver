package chatserver;

/*
 * Defined in Client, on 8/12/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: 1.0
 *
 * Summary: -
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import network.Connect;
import network.Packet;

import java.io.IOException;

public class Handler implements Runnable {

    private Connect connect;
    private Module module;
    private String handle;
    private String abbr;

    private boolean moduleThreadRunning = false;
    private Thread moduleThread;

    public Handler(Module module, String handle, String abbr) {
        this.module = module;
        this.handle = handle;
        this.abbr = abbr;

        connect = new Connect(new Network());

        try {
            connect.createServerConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected boolean startModuleThread() {
        if (moduleThreadRunning) return false;

        moduleThread = new Thread(module);

        moduleThread.start();

        return moduleThreadRunning = true;
    }

    @Override
    public void run() {
        Packet packet;

        while (true) {
            packet = connect.receive();

            if(packet != null){
                System.out.println(packet.toString());
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
