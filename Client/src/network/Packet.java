package network;

/*
 * Packet.java, on 7/23/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: -
 *
 * Summary: -
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class Packet {

    private ByteBuffer packetContent;

    public abstract void lock() throws IOException;

    public abstract void unlock() throws IOException;

    public void writeString(String value) throws IOException {
        if (value.length() > Integer.MAX_VALUE) {
            throw new IOException("Invalid string length");
        }

        for (int i = 0; i < value.length(); i++) {
            packetContent.put((byte) value.charAt(i));
        }
    }

    public String readString(ByteBuffer buffer) throws IOException {
        if (packetContent.limit() == packetContent.capacity()) packetContent.flip();

        char[] chars = new char[packetContent.limit()];

        for (int i = 0; i < packetContent.limit(); i++) {
            chars[i] = packetContent.getChar();
        }

        return new String(chars);
    }

    public String toString() {
        return packetContent.toString();
    }
}
