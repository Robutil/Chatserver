package network;

/*
 * Connect.java, on 7/14/2016.
 *
 * Robbin Siepman <Robbin@Siepman.com>
 * Copyright (c) 2016 <Robutil>
 *
 * Version: -
 *
 * Summary: -
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFINGEMENT. IN NO EVENT SHALL THE AUTHOR(S) OR COPYRIGHT
 * HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SocketChannel;

/**
 * This class is used to connect to a (remote) host. On calling
 * the toServer() method it tries to establish a connection with
 * the parameters provided earlier. Default values are: localhost,
 * 52020. If a connection cannot be established or a connection
 * is lost it will throw an IOException.
 */

public class Connect {

    //Connection information
    private String address;
    private int port;
    private volatile boolean connected;
    private boolean blocking;

    //private Selector flowControl;
    private SocketChannel serverSocket;

    private ByteBuffer inputStreamBuffer;
    private Protocol protocol;

    /**
     * Initialising a Connect object allocates buffers for sending
     * and receiving data. The byte order is set accordingly.
     */
    public Connect(Protocol protocol) {

        this.protocol = protocol;

        //inputStreamBuffer size, amounts to 4096. Keep a multiple of 2.
        int bufferSize = 1 << 12;

        //Buffer to store incoming messages
        this.inputStreamBuffer = ByteBuffer.allocate(bufferSize);
        this.inputStreamBuffer.order(ByteOrder.LITTLE_ENDIAN);

        //Default values for a connection
        this.address = "localhost";
        this.port = 52020;
        this.blocking = true;
    }

    /**
     * Sets the port to connect to. Calling this function after a
     * connection has already been established will have no effect.
     * Address will default to localhost if not initialised.
     *
     * @param port: Server port to connect to. Preferable range
     *              this number from 49152 to 65535.
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Sets the address to connect to. Calling this function after a
     * connection has already been established will have no effect.
     * Port will default to 52020 if not initialised.
     *
     * @param address: Server ipv4 address to connect to.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Sets the address and port to connect to. Calling this function after a
     * connection has already been established will have no effect.
     *
     * @param address: The ipv4 address to connect to. Default is localhost.
     * @param port:    The specified port. Defaults to 52020.
     */
    public void setAddress(String address, int port) {
        this.address = address;
        this.port = port;
    }

    /** Sets blocking characteristic for channel */
    public void setBlocking(boolean blocking) {
        this.blocking = blocking;
    }

    /**
     * Creates a connection with the server
     *
     * @throws IOException: On connection lost.
     */
    public boolean createServerConnection() throws IOException {

        //Create a connection to the server
        serverSocket = SocketChannel.open();
        serverSocket.configureBlocking(blocking);

        connected = serverSocket.connect(new InetSocketAddress(address, port));

        if (!connected) {
            //Connection to server failed.
            return false;
        }

        serverSocket.socket().setTcpNoDelay(true);
        serverSocket.socket().setSoTimeout(10000);

        //Got a valid connection with the server
        return serverSocket.isConnected();
    }

    /**
     * Sends outputStreamBuffer over channel
     */
    public boolean send(Packet packet) {

//        if (!connected) return false;
//
//        ByteBuffer outputStreamBuffer = new ByteBuffer();
//        protocol.addPacket(null, null);
//
//        if (outputStreamBuffer.limit() > 0) {
//            try {
//                serverSocket.write(outputStreamBuffer);
//            } catch (IOException e) {
//                connected = false;
//                e.printStackTrace();
//            }
//        }
//
//        outputStreamBuffer.flip();
     return true;
    }

    /**
     * Receives input from channel
     */
    public Packet receive() {

        if (!connected) return null;

        try {
            /* Attempt to read data from inputStreamBuffer */
            int bytesRead = serverSocket.read(inputStreamBuffer);

            if (bytesRead <= 0) {

                /* Connection lost */
                connected = false;
                throw new IllegalStateException("Lost connection to server");
            }

        } catch (IOException ex) {

            /* Connection lost */
            throw new IllegalStateException("Could not get server data", ex);
        }

        if (inputStreamBuffer.position() != 0) inputStreamBuffer.flip();

        return protocol.readPacket(inputStreamBuffer);
    }
}
